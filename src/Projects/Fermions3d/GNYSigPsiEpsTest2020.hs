{-# LANGUAGE DataKinds             #-}
{-# LANGUAGE DeriveAnyClass        #-}
{-# LANGUAGE DeriveGeneric         #-}
{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE FlexibleContexts      #-}
{-# LANGUAGE LambdaCase            #-}
{-# LANGUAGE OverloadedStrings     #-}
{-# LANGUAGE PolyKinds             #-}
{-# LANGUAGE RecordWildCards       #-}
{-# LANGUAGE ScopedTypeVariables   #-}
{-# LANGUAGE StaticPointers        #-}
{-# LANGUAGE TypeFamilies          #-}

module Projects.Fermions3d.GNYSigPsiEpsTest2020 where

import qualified Blocks.Blocks3d                      as B3d
import qualified Bootstrap.Bounds.Spectrum            as Spectrum
import           Bootstrap.Math.AffineTransform       (AffineTransform (..),
                                                       apply)
import           Bootstrap.Math.Linear                (Matrix, V, fromV, toM,
                                                       toV)
import           Bounds.Fermions3d.GNY                (ChannelType (..))
import qualified Bounds.Fermions3d.GNYSigPsiEps       as GNYSigPsiEps
import           Bounds.Scalars3d.ONRep               (ONRep (..))
import           Control.Monad                        (forM_, void, when)
import           Control.Monad.IO.Class               (liftIO)
import           Control.Monad.Reader                 (asks, local)
import           Data.List.Split                      (chunksOf)
import qualified Data.Map.Strict                      as Map
import           Data.Text                            (Text)
import           Data.Time.Clock                      (NominalDiffTime)
import           Hyperion
import           Hyperion.Bootstrap.Bound             (Bound (..),
                                                       CheckpointMap, LambdaMap)
import qualified Hyperion.Bootstrap.Bound             as Bound
import           Hyperion.Bootstrap.DelaunaySearch    (DelaunayConfig (..),
                                                       delaunaySearchRegionPersistent)
import           Hyperion.Bootstrap.Main              (unknownProgram)
import           Hyperion.Bootstrap.OPESearch         (BilinearForms (..),
                                                       OPESearchConfig (..))
import qualified Hyperion.Bootstrap.OPESearch         as OPE
import qualified Hyperion.Bootstrap.Params            as Params
import qualified Hyperion.Bootstrap.SDPDeriv          as SDPDeriv
import qualified Hyperion.Bootstrap.SDPDeriv.BFGS     as BFGS
import qualified Hyperion.Database                    as DB
import qualified Hyperion.Log                         as Log
import qualified Hyperion.Slurm                       as Slurm
import           Hyperion.Util                        (hour, sanitizeFileString)
import           Numeric.Rounded                      (Rounded,
                                                       RoundingMode (..))
import           Projects.Fermions3d.Defaults         (defaultBoundConfig,
                                                       defaultDelaunayConfig,
                                                       defaultQuadraticNetConfig)
import qualified Projects.Fermions3d.GNYData2020Chi35 as GNYData
import qualified Projects.Fermions3d.GNYN4Data        as N4
import qualified Projects.Fermions3d.GNYN8Data        as N8
import qualified SDPB
import           SDPB                                 (Params (..))
import           System.Directory                     (removePathForcibly)
import           System.FilePath.Posix                ((</>))


-- remoteComputeGNYSigPsiEpsBound :: Bound Int GNYSigPsiEps.GNYSigPsiEps -> Cluster SDPB.Output
-- remoteComputeGNYSigPsiEpsBound bound = do
--   workDir <- newWorkDir bound
--   jobTime <- asks (Slurm.time . clusterJobOptions)
--   result <-
--     continueSDPBCheckpointed (workDir, solverParams bound) $
--     remoteEvalJob (static (remoteFnJob compute)) (jobTime, bound, workDir)
--   Log.info "Computed" (bound, result)
--   DB.insert (DB.KeyValMap "computations") bound result
--   return result
--   where
--     compute (jobTime', bound', workDir') = do
--       -- We ask SDPB to terminate within 90% of the jobTime. This
--       -- should ensure sufficiently prompt exit as long as an
--       -- iteration doesn't take 10% or more of the jobTime.
--       solverParams' <- liftIO $ Bound.setFixedTimeLimit (0.9*jobTime') (solverParams bound')
--       computeClean bound' { solverParams = solverParams' } workDir'

remoteGNYSigPsiEpsOPESearch
  :: CheckpointMap GNYSigPsiEps.GNYSigPsiEps
  -> LambdaMap GNYSigPsiEps.GNYSigPsiEps (V 4 Rational)
  -> BilinearForms 4
  -> Bound Int GNYSigPsiEps.GNYSigPsiEps
  -> Cluster Bool
remoteGNYSigPsiEpsOPESearch checkpointMap lambdaMap initialBilinearForms =
  OPE.remoteOPESearch (cPtr (static opeSearchCfg)) checkpointMap lambdaMap initialBilinearForms
  where
    opeSearchCfg =
      OPESearchConfig setLambda GNYSigPsiEps.getExternalMat (OPE.queryAllowedMixed qmConfig)
    setLambda :: (V 4 Rational) -> Bound prec GNYSigPsiEps.GNYSigPsiEps -> Bound prec GNYSigPsiEps.GNYSigPsiEps
    setLambda lambda bound = bound
      { boundKey = (boundKey bound)
        { GNYSigPsiEps.objective = GNYSigPsiEps.Feasibility (Just lambda) }
      }
    qmConfig = OPE.QueryMixedConfig
      { qmQuadraticNetConfig = defaultQuadraticNetConfig
      , qmDescentConfig      = OPE.defaultDescentConfig
      , qmResolution         = 1e-16
      , qmPrecision          = 384
      , qmHessianLineSteps   = 200
      , qmHessianLineAverage = 10
      }

remoteComputeGNYSigPsiEpsBounds :: [Bound Int GNYSigPsiEps.GNYSigPsiEps] -> Cluster ()
remoteComputeGNYSigPsiEpsBounds bounds = do
  -- Wait between 15 seconds and an hour. Hopefully this relieves
  -- pressure on the database when submitting a large number of
  -- jobs...
  -- t <- liftIO $ randomRIO (15, 3600)
  -- liftIO $ threadDelay $ t*1000*1000
  workDir <- newWorkDir (head bounds)
  jobTime <- asks (Slurm.time . clusterJobOptions)
  remoteEvalJob $
    static compute `ptrAp` cPure jobTime `cAp` cPure bounds `cAp` cPure workDir
  where
    computationsMap :: DB.KeyValMap (Bound Int GNYSigPsiEps.GNYSigPsiEps) SDPB.Output
    computationsMap = DB.KeyValMap "computations"
    compute jobTime bounds' workDir' = do
      -- We ask SDPB to terminate within 90% of the jobTime. This
      -- should ensure sufficiently prompt exit as long as an
      -- iteration doesn't take 10% or more of the jobTime.
      solverParams' <- liftIO $ Bound.setFixedTimeLimit (0.9*jobTime) (solverParams (head bounds'))
      forM_ (zip bounds' [1 :: Int ..]) $ \(bound,i) ->
        DB.lookup computationsMap bound >>= \case
          Just _  -> pure ()
          Nothing -> do
            Log.text $ "Computing bound " <> Log.showText i <> " of " <> Log.showText (length bounds') <> "."
            let boundFiles = Bound.defaultBoundFiles workDir'
            result <- Bound.computeClean' (bound { solverParams = solverParams' }) boundFiles
            when (SDPB.isFinished result) $
              DB.insert computationsMap bound result
            liftIO $ mapM_ removePathForcibly [Bound.checkpointDir boundFiles, Bound.outDir boundFiles]

-- | Convert a 'Bound prec GNY' to a 3-dimensional vector given by
--
-- (deltaPsi, deltaSig, deltaEps)
--
-- It is important that the ordering convention for 3-dimensional
-- vectors of dimensions be kept consistent in all computations.
deltaExtToV :: Bound prec GNYSigPsiEps.GNYSigPsiEps -> V 3 Rational
deltaExtToV Bound { boundKey = GNYSigPsiEps.GNYSigPsiEps { externalDims = eds } } =
  toV (GNYSigPsiEps.deltaPsi eds, GNYSigPsiEps.deltaSig eds, GNYSigPsiEps.deltaEps eds)

psiChannel, sigChannel, epsChannel, chiChannel, sigTChannel, epsTChannel :: ChannelType
psiChannel  = ChannelType (1/2) B3d.ParityEven ONVector
sigChannel  = ChannelType 0 B3d.ParityOdd ONSinglet
epsChannel  = ChannelType 0 B3d.ParityEven ONSinglet
chiChannel  = ChannelType (1/2) B3d.ParityOdd ONVector
sigTChannel = ChannelType 0 B3d.ParityOdd ONSymTensor
epsTChannel = ChannelType 0 B3d.ParityEven ONSymTensor

gnySigPsiEpsFeasibleDefaultGaps :: V 3 Rational -> Maybe (V 4 Rational) -> Rational -> Int -> GNYSigPsiEps.GNYSigPsiEps
gnySigPsiEpsFeasibleDefaultGaps deltaExts mLambda ngroup nmax = GNYSigPsiEps.GNYSigPsiEps
  { spectrum     = Spectrum.setTwistGap 1e-6 $ Spectrum.setGaps
    [ (psiChannel, 2)
    , (chiChannel, 3.5) --3.5)
    , (sigChannel, 2.5) --2.5)
    , (epsChannel, 3) --3)
    , (sigTChannel, 2)
      -- , (epsTChannel, 3)
    ] Spectrum.unitarySpectrum
  , nGroup       = ngroup
  , objective    = GNYSigPsiEps.Feasibility mLambda
  , externalDims = GNYSigPsiEps.ExternalDims {..}
  , blockParams  = Params.block3dParamsNmax nmax
  , spins        = Params.gnyspinsNmax nmax
  }
  where
    (deltaPsi, deltaSig, deltaEps) = fromV deltaExts

gnySigPsiEpsGFFNavigatorDefaultGaps :: V 3 Rational -> Maybe (V 4 Rational) -> Rational -> Int -> GNYSigPsiEps.GNYSigPsiEps
gnySigPsiEpsGFFNavigatorDefaultGaps deltaExts mLambda ngroup nmax = GNYSigPsiEps.GNYSigPsiEps
  { spectrum     = Spectrum.setTwistGap 1e-6 $ Spectrum.setGaps
    [ (psiChannel, 2)
    , (chiChannel, 3.5) --3.5)
    , (sigChannel, 2.5) --3)
    , (epsChannel, 3) --3)
    , (sigTChannel, 2)
    ] Spectrum.unitarySpectrum
  , nGroup       = ngroup
  , objective    = GNYSigPsiEps.GFFNavigator mLambda
  , externalDims = GNYSigPsiEps.ExternalDims {..}
  , blockParams  = Params.block3dParamsNmax nmax
  , spins        = Params.gnyspinsNmax nmax
  }
  where
    (deltaPsi, deltaSig, deltaEps) = fromV deltaExts

gnyLooseFeasibleDefaultGaps :: V 3 Rational -> Maybe (V 4 Rational) -> Rational -> Int -> GNYSigPsiEps.GNYSigPsiEps
gnyLooseFeasibleDefaultGaps deltaExts mLambda ngroup nmax = GNYSigPsiEps.GNYSigPsiEps
  { spectrum     = Spectrum.setTwistGap 1e-6 $ Spectrum.setGaps
    [ (psiChannel, 2)
    , (chiChannel, 2)
    , (sigChannel, 2)
    , (epsChannel, 2)
      -- , (sigTChannel, 2)
      -- , (epsTChannel, 3)
    ] Spectrum.unitarySpectrum
  , nGroup       = ngroup
  , objective    = GNYSigPsiEps.Feasibility mLambda
  , externalDims = GNYSigPsiEps.ExternalDims {..}
  , blockParams  = Params.block3dParamsNmax nmax
  , spins        = Params.gnyspinsNmax nmax -- gynspinsNmax output [Half-int], defined in ParamCollection.hs
  }
  where
    (deltaPsi, deltaSig, deltaEps) = fromV deltaExts

gny2dFeasibleDefaultGaps :: V 2 Rational -> Maybe (V 4 Rational) -> Rational -> Int -> GNYSigPsiEps.GNYSigPsiEps
gny2dFeasibleDefaultGaps deltaExts mLambda ngroup nmax = GNYSigPsiEps.GNYSigPsiEps
  { spectrum     = Spectrum.setTwistGap 1e-6 $ Spectrum.setGaps
    [ (psiChannel, 2)
    , (chiChannel, 3.5)
    , (sigChannel, 3)
    , (epsChannel, 3)
    , (sigTChannel, 2)
      -- , (epsTChannel, 3)
    ] Spectrum.unitarySpectrum
  , nGroup       = ngroup
  , objective    = GNYSigPsiEps.Feasibility mLambda
  , externalDims = GNYSigPsiEps.ExternalDims {..}
  , blockParams  = Params.block3dParamsNmax nmax
  , spins        = Params.gnyspinsNmax nmax
  }
  where
    (deltaPsi, deltaSig) = fromV deltaExts
    deltaEps = 1.65

gny4dFeasibleDefaultGaps :: V 4 Rational -> Maybe (V 4 Rational) -> Rational -> Int -> GNYSigPsiEps.GNYSigPsiEps
gny4dFeasibleDefaultGaps deltaExts mLambda ngroup nmax = GNYSigPsiEps.GNYSigPsiEps
  { spectrum     = Spectrum.setGaps
    [ (psiChannel, 2)
    , (chiChannel, 3.5)
    , (sigChannel, deltaSigP)
    , (epsChannel, 3)
    , (sigTChannel, 2)
      -- , (epsTChannel, 3)
    ] Spectrum.unitarySpectrum
  , nGroup       = ngroup
  , objective    = GNYSigPsiEps.Feasibility mLambda
  , externalDims = GNYSigPsiEps.ExternalDims {..}
  , blockParams  = Params.block3dParamsNmax nmax
  , spins        = Params.gnyspinsNmax nmax
  }
  where
    (deltaPsi, deltaSig, deltaEps, deltaSigP) = fromV deltaExts

gnySigPsiEpsSigPGFFNavigatorDefaultGaps :: V 4 Rational -> Maybe (V 4 Rational) -> Rational -> Int -> GNYSigPsiEps.GNYSigPsiEps
gnySigPsiEpsSigPGFFNavigatorDefaultGaps deltaExts mLambda ngroup nmax = GNYSigPsiEps.GNYSigPsiEps
  { spectrum     = Spectrum.setTwistGap 1e-6 $ Spectrum.setGaps
    [ (psiChannel, 2)
    , (chiChannel, 3.5) --3.5)
    , (sigChannel, deltaSigP) --3)
    , (epsChannel, 3) --3)
    , (sigTChannel, 2)
    ] Spectrum.unitarySpectrum
  , nGroup       = ngroup
  , objective    = GNYSigPsiEps.GFFNavigator mLambda
  , externalDims = GNYSigPsiEps.ExternalDims {..}
  , blockParams  = Params.block3dParamsNmax nmax
  , spins        = Params.gnyspinsNmax nmax
  }
  where
    (deltaPsi, deltaSig, deltaEps, deltaSigP) = fromV deltaExts


delaunaySearchPoints :: DB.KeyValMap (V n Rational) (Maybe Bool)
delaunaySearchPoints = DB.KeyValMap "delaunaySearchPoints"


data GNYDelaunaySearchData = GNYDelaunaySearchData
  { nmax              :: Int
  , affine            :: AffineTransform 3 Rational
  , initialLambda     :: V 4 Rational
  , initialCheckpoint :: Maybe FilePath
  , initialDisallowed :: [V 3 Rational]
  , initialAllowed    :: [V 3 Rational]
  , opeEllipse        :: Matrix 4 4 Rational
  , solverPrecision   :: Int
  , delaunayConfig    :: DelaunayConfig
  , jobTime           :: NominalDiffTime
  , jobType           :: MPIJob
  , jobMemory         :: Text
  }

gnyDelaunaySearchOPEScan :: Rational -> GNYDelaunaySearchData -> Cluster ()
gnyDelaunaySearchOPEScan nGroup GNYDelaunaySearchData{..} =
  local (setJobType jobType . setJobTime jobTime . setJobMemory jobMemory . setSlurmPartition "compute") $ void $ do
  checkpointMap <- Bound.newCheckpointMap affine deltaExtToV initialCheckpoint
  lambdaMap     <- Bound.newLambdaMap affine deltaExtToV (Just initialLambda)
  delaunaySearchRegionPersistent delaunaySearchPoints delaunayConfig affine initialPts
    (remoteGNYSigPsiEpsOPESearch checkpointMap lambdaMap
     (BilinearForms 1e-32 [(Nothing, opeEllipse)]) . bound)
  where
    bound v = Bound
      { boundKey = gnySigPsiEpsFeasibleDefaultGaps v (Just initialLambda) nGroup nmax
      , precision = solverPrecision
      , solverParams = (Params.jumpFindingParams nmax) { precision = solverPrecision
                                                , procGranularity = 32 }
      , boundConfig = defaultBoundConfig
      }
    initialPts = Map.fromList $
      [(p, Just True)  | p <- initialAllowed] ++
      [(p, Just False) | p <- initialDisallowed]


gny4Affine3dNmax6 :: AffineTransform 3 Rational
gny4Affine3dNmax6 = AffineTransform
  { affineShift  = toV ( 1.0456, 0.742559, 1.50274)
  , affineLinear = toV ( toV (-0.00289182, 0.0286252, 0.527533)
                       , toV (-0.000864904, -0.0227698, 0.00123081)
                       , toV (0.00281221, -0.000105677, 0.0000211502)
                       )
  }


boundsProgram :: Text -> Cluster ()

-------------------
-- N=2 3d search --
-------------------

boundsProgram "GNYSigPsiEps_test_ngroup2_nmax6" =
  local (setJobType (MPIJob 2 24) . setJobTime (8*hour) . setSlurmPartition "compute") $ void $ do
  mapConcurrently_ (Bound.remoteCompute . bound) $
     fmap toV $ [(1.0686, 0.651, 1.70)] --n2nmax6PrimalPoints --GNYData.epsN2nmax6DualPoints --[(1.06 + x/200, 0.6 + y/50, 1.3 + z/10) | x <-[0..4], y <- [0..5], z <- [0..7]]
  where
    nmax = 6
    ngroup = 2
    bound deltaExts = Bound
      { boundKey = gnySigPsiEpsFeasibleDefaultGaps deltaExts Nothing ngroup nmax
      , precision = B3d.precision (Params.block3dParamsNmax nmax)
      , solverParams = (Params.jumpFindingParams nmax) { precision = 768
                                             , dualErrorThreshold=1e-30
                                             , primalErrorThreshold=1e-40
                                             , findPrimalFeasible = False }
      , boundConfig = defaultBoundConfig
      }

boundsProgram "GNYSigPsiEps_test_ngroup2_nmax8" =
  local (setJobType (MPIJob 1 24) . setJobTime (8*hour)) $ void $ do
  mapConcurrently_ (Bound.remoteCompute . bound) $
     fmap toV GNYData.epsN2nmax8DualPoints --GNYData.epsN2nmax6DualPoints --[(1.06 + x/200, 0.6 + y/50, 1.3 + z/10) | x <-[0..4], y <- [0..5], z <- [0..7]]
  where
    nmax = 8
    ngroup = 2
    bound deltaExts = Bound
      { boundKey = gnySigPsiEpsFeasibleDefaultGaps deltaExts Nothing ngroup nmax
      , precision = B3d.precision (Params.block3dParamsNmax nmax)
      , solverParams = (Params.jumpFindingParams nmax) { precision = 768
                                             , dualErrorThreshold=1e-30
                                             , primalErrorThreshold=1e-40
                                             , findPrimalFeasible = False }
      , boundConfig = defaultBoundConfig
      }

boundsProgram "GNYSigPsiEps_test_ngroup2_nmax10" =
  local (setJobType (MPIJob 1 36) . setJobTime (12*hour)) $ void $ do
  mapConcurrently_ (Bound.remoteCompute . bound) $
     fmap toV GNYData.epsN2nmax10PrimalPoints --GNYData.epsN2nmax6DualPoints --[(1.06 + x/200, 0.6 + y/50, 1.3 + z/10) | x <-[0..4], y <- [0..5], z <- [0..7]]
  where
    nmax = 10
    ngroup = 2
    bound deltaExts = Bound
      { boundKey = gnySigPsiEpsFeasibleDefaultGaps deltaExts Nothing ngroup nmax
      , precision = B3d.precision (Params.block3dParamsNmax nmax)
      , solverParams = (Params.jumpFindingParams nmax) { precision = 768
                                             , dualErrorThreshold=1e-30
                                             , primalErrorThreshold=1e-40
                                             , findPrimalFeasible = False }
      , boundConfig = defaultBoundConfig
      }


boundsProgram "GNY2d_test_ngroup2_nmax6" =
  local (setJobType (MPIJob 1 8) . setJobTime (8*hour)) $ void $ do
  mapConcurrently_ (Bound.remoteCompute . bound) $
     fmap toV [(1.068, 0.65), (1.069, 0.66), (1.071, 0.645)]
  where
    nmax = 6
    ngroup = 2
    bound deltaExts = Bound
      { boundKey = gny2dFeasibleDefaultGaps deltaExts Nothing ngroup nmax
      , precision = B3d.precision (Params.block3dParamsNmax nmax)
      , solverParams = (Params.jumpFindingParams nmax) { precision = 768
                                             , findPrimalFeasible = False }
      , boundConfig = defaultBoundConfig
      }



boundsProgram "GNY4d_test_ngroup2_nmax6" =
  local (setJobType (MPIJob 1 12) . setJobTime (8*hour)) $ void $ do
  mapConcurrently_ (Bound.remoteCompute . bound) $
     fmap toV [(1.06 + x/250, 0.64 + y/50, 1.3 + z/10, 2.6 + s/10) | x <-[0..3], y <- [0..4], z <- [0..5], s <- [0..4]]
  where
    nmax = 6
    ngroup = 2
    bound deltaExts = Bound
      { boundKey = gny4dFeasibleDefaultGaps deltaExts Nothing ngroup nmax
      , precision = B3d.precision (Params.block3dParamsNmax nmax)
      , solverParams = (Params.jumpFindingParams nmax) { precision = 768
                                             , findPrimalFeasible = False }
      , boundConfig = defaultBoundConfig
      }

boundsProgram "GNYSigPsiEp_2d_island_ngroup2_nmax6" =
 local (setJobType (MPIJob 1 8) . setJobTime (8*hour)) $ do
  void $ delaunaySearchRegionPersistent delaunaySearchPoints delaunayConfig affine initialPts f
  where
    nmax = 6
    ngroup = 2
    affine = GNYData.gny2AffineNmax6
    f :: V 2 Rational -> Cluster Bool
    f deltaExts = fmap SDPB.isPrimalFeasible (Bound.remoteCompute $ bound deltaExts)
    bound deltaExts = Bound
      { boundKey = gny2dFeasibleDefaultGaps deltaExts Nothing ngroup nmax
      , precision = B3d.precision (Params.block3dParamsNmax nmax)
      , solverParams = (Params.jumpFindingParams nmax) { precision = 768
                                             , findPrimalFeasible = False }
      , boundConfig = defaultBoundConfig
      }
    delaunayConfig = defaultDelaunayConfig 10 100
    initialPts = Map.fromList $
      [(p, Just True)  | p <- initialAllowed] -- ++ [(p, Just False) | p <- initialDisallowed]
      where
        --initialDisallowed = fmap toV $ [()]--GNYData.n2nmax6DualPoints
        initialAllowed = fmap toV $ [(1.068, 0.65), (1.069, 0.66), (1.071, 0.645)]




boundsProgram "GNYSigPsiEps_island_ngroup2_nmax6" =
 local (setJobType (MPIJob 1 16) . setJobTime (8*hour)) $ do
  void $ delaunaySearchRegionPersistent delaunaySearchPoints delaunayConfig affine initialPts f
  where
    nmax = 6
    ngroup = 2
    affine = GNYData.gny2Affine3dWindow
    f :: V 3 Rational -> Cluster Bool
    f deltaExts = fmap SDPB.isPrimalFeasible (Bound.remoteCompute $ bound deltaExts)
    bound deltaExts = Bound
      { boundKey = gnySigPsiEpsFeasibleDefaultGaps deltaExts Nothing ngroup nmax
      , precision = B3d.precision (Params.block3dParamsNmax nmax)
      , solverParams = (Params.jumpFindingParams nmax) { precision = 768
                                             , dualErrorThreshold=1e-30
                                             , primalErrorThreshold=1e-40
                                             , findPrimalFeasible = False }
      , boundConfig = defaultBoundConfig
      }
    delaunayConfig = defaultDelaunayConfig 20 200
    initialPts = Map.fromList $
      [(p, Just True)  | p <- initialAllowed] ++ [(p, Just False) | p <- initialDisallowed]
      where
        initialDisallowed = fmap toV $ GNYData.epsN2nmax6DualPoints
        initialAllowed = fmap toV $ GNYData.epsN2nmax6PrimalPoints

boundsProgram "GNYSigPsiEps_island_ngroup2_nmax8" =
 local (setJobType (MPIJob 1 24) . setJobTime (12*hour)) $ do
  void $ delaunaySearchRegionPersistent delaunaySearchPoints delaunayConfig affine initialPts f
  where
    nmax = 8
    ngroup = 2
    affine = GNYData.gny2Affine3dWindow
    f :: V 3 Rational -> Cluster Bool
    f deltaExts = fmap SDPB.isPrimalFeasible (Bound.remoteCompute $ bound deltaExts)
    bound deltaExts = Bound
      { boundKey = gnySigPsiEpsFeasibleDefaultGaps deltaExts Nothing ngroup nmax
      , precision = B3d.precision (Params.block3dParamsNmax nmax)
      , solverParams = (Params.jumpFindingParams nmax) { precision = 768
                                             , dualErrorThreshold=1e-30
                                             , primalErrorThreshold=1e-40
                                             , findPrimalFeasible = False }
      , boundConfig = defaultBoundConfig
      }
    delaunayConfig = defaultDelaunayConfig 20 200
    initialPts = Map.fromList $
      [(p, Just True)  | p <- initialAllowed] ++ [(p, Just False) | p <- initialDisallowed]
      where
        initialDisallowed = fmap toV $ GNYData.epsN2nmax8DualPoints
        initialAllowed = fmap toV $ GNYData.epsN2nmax8PrimalPoints



boundsProgram "GNYSigPsiEp_island_ngroup2_nmax6_with_checkpoint_map" =
  -- To run at Yale, probably best to set 8 CPUs -> 7 CPUs and 24GB -> 20GB
  local (setJobType (MPIJob 1 12) . setJobMemory "36G" . setJobTime (8*hour)) $ do
  checkpointMap <- Bound.newCheckpointMap affine deltaExtToV mInitialCheckpoint
  let
    f :: V 3 Rational -> Cluster Bool
    f deltaV = fmap SDPB.isPrimalFeasible $
      Bound.remoteComputeWithCheckpointMap checkpointMap $
      bound deltaV
  void $ delaunaySearchRegionPersistent delaunaySearchPoints delaunayConfig affine initialPts f
  where
    nmax = 6
    ngroup = 2
    affine = GNYData.gny2Affine3dWindow
    bound deltaV = Bound
      { boundKey = gnySigPsiEpsFeasibleDefaultGaps deltaV Nothing ngroup nmax
      , precision = B3d.precision (Params.block3dParamsNmax nmax)
      , solverParams = (Params.jumpFindingParams nmax) { precision = 768
                                             , dualErrorThreshold=1e-30
                                             , primalErrorThreshold=1e-40
                                             , findPrimalFeasible = False }
      , boundConfig = defaultBoundConfig
      }
    delaunayConfig = defaultDelaunayConfig 20 400
    initialPts = Map.fromList $
      [(p, Just True)  | p <- initialAllowed] ++ [(p, Just False) | p <- initialDisallowed]
      where
        initialDisallowed = fmap toV $ GNYData.epsN2nmax6DualPoints
        initialAllowed = fmap toV $ GNYData.epsN2nmax6PrimalPoints
    -- Instead of 'Nothing', we could supply a pre-computed checkpoint
    -- here. It would be used as the default return value for the
    -- checkpointMap, until at least one completed checkpoint gets
    -- recorded.
    mInitialCheckpoint = Nothing


boundsProgram "GNYSigPsiEp_island_ngroup2_nmax8_with_checkpoint_map" =
  -- To run at Yale, probably best to set 8 CPUs -> 7 CPUs and 24GB -> 20GB
  local (setJobType (MPIJob 1 24) . setJobMemory "72G" . setJobTime (15*hour)) $ do
  checkpointMap <- Bound.newCheckpointMap affine deltaExtToV mInitialCheckpoint
  let
    f :: V 3 Rational -> Cluster Bool
    f deltaV = fmap SDPB.isPrimalFeasible $
      Bound.remoteComputeWithCheckpointMap checkpointMap $
      bound deltaV
  void $ delaunaySearchRegionPersistent delaunaySearchPoints delaunayConfig affine initialPts f
  where
    nmax = 8
    ngroup = 2
    affine = GNYData.gny2Affine3dWindow
    bound deltaV = Bound
      { boundKey = gnySigPsiEpsFeasibleDefaultGaps deltaV Nothing ngroup nmax
      , precision = B3d.precision (Params.block3dParamsNmax nmax)
      , solverParams = (Params.jumpFindingParams nmax) { precision = 768
                                             , dualErrorThreshold=1e-30
                                             , primalErrorThreshold=1e-40
                                             , findPrimalFeasible = False }
      , boundConfig = defaultBoundConfig
      }
    delaunayConfig = defaultDelaunayConfig 20 200
    initialPts = Map.fromList $
      [(p, Just True)  | p <- initialAllowed] ++ [(p, Just False) | p <- initialDisallowed]
      where
        initialDisallowed = fmap toV $ GNYData.epsN2nmax8DualPoints
        initialAllowed = fmap toV $ GNYData.epsN2nmax8PrimalPoints
    -- Instead of 'Nothing', we could supply a pre-computed checkpoint
    -- here. It would be used as the default return value for the
    -- checkpointMap, until at least one completed checkpoint gets
    -- recorded.
    mInitialCheckpoint = Nothing

--------------
-- N=2 OPE ---
--------------

boundsProgram "GNYSigPsiEps_OPEtest_ngroup2_nmax6" =
  local (setJobType (MPIJob 1 18) . setJobTime (8*hour)) $ void $ do
  mapConcurrently_ (Bound.remoteCompute . bound) $
     fmap toV GNYData.epsN2nmax6PrimalPoints --n2nmax6PrimalPoints --GNYData.epsN2nmax6DualPoints --[(1.06 + x/200, 0.6 + y/50, 1.3 + z/10) | x <-[0..4], y <- [0..5], z <- [0..7]]
  where
    nmax = 6
    ngroup = 2
    mLambda :: Maybe (V 4 Rational)
    mLambda = Just $ toV (0,1,1,1)
    bound deltaExts = Bound
      { boundKey = gnySigPsiEpsFeasibleDefaultGaps deltaExts mLambda ngroup nmax
      , precision = B3d.precision (Params.block3dParamsNmax nmax)
      , solverParams = (Params.jumpFindingParams nmax) { precision = 768
                                             , findPrimalFeasible = False }
      , boundConfig = defaultBoundConfig
      }

boundsProgram "GNYSigPsiEps_OPE_LambdaTest_ngroup2_nmax4" =
  local (setJobType (MPIJob 1 18) . setJobTime (8*hour)) $ void $ do
  mapConcurrently_ (Bound.remoteCompute . bound) $
     fmap toV [(0,1,0,0),(0,0,1,0)]
  where
    nmax = 4
    ngroup = 2
    -- mLambda :: Maybe (V 4 Rational)
    -- mLambda = Just $ toV (0,1,1,1)
    deltaExts = toV (1.069,0.65,1.75)
    bound lambda = Bound
      { boundKey = gnySigPsiEpsFeasibleDefaultGaps deltaExts (Just lambda) ngroup nmax
      , precision = B3d.precision (Params.block3dParamsNmax nmax)
      , solverParams = (Params.jumpFindingParams nmax) { precision = 768
                                             , findPrimalFeasible = False }
      , boundConfig = defaultBoundConfig
      }


boundsProgram "GNYSigPsiEps_OPESearchTest_ngroup2_nmax6" =
  local (setJobType (MPIJob 1 18) . setJobTime (8*hour)) $ void $ do
  checkpointMap <- Bound.newCheckpointMap affine deltaExtToV Nothing
  lambdaMap     <- Bound.newLambdaMap affine deltaExtToV Nothing
  mapConcurrently_ (remoteGNYSigPsiEpsOPESearch checkpointMap lambdaMap initialBilinearForms . bound) $
     fmap toV [(1.0686, 0.651, 1.70)] --n2nmax6PrimalPoints --GNYData.epsN2nmax6DualPoints --[(1.06 + x/200, 0.6 + y/50, 1.3 + z/10) | x <-[0..4], y <- [0..5], z <- [0..7]]
  where
    nmax = 6
    ngroup = 2
    affine = GNYData.gny2Affine3dWindow--gny2Affine3dNmax6
    bilinearMat :: Matrix 4 4 Rational
    bilinearMat = toM
     ( (2226.2016, -2181.1580, -693.6937, 53.1024)
     , (-2181.1580, 4852.3925, -20.5093, -28.4479)
     , (-693.6937, - 20.5093, 413.5043, -33.8579)
     , (53.1024, -28.4479, -33.8579, 7.4766)
     )
--      ( (20.0, 0, 0, -8.0)
--      , (0, 20.0, 0, -3.5)
--      , (0, 0, 20,  -14.5)
--      , (-8.0, -3.5, -14.5, 5.98)
--      )
--     ( (20.0, 0, 0, -16.0)
--      , (0, 20.0, 0, -11.5)
--      , (0, 0, 20,  -34.5)
--      , (-16.0, -11.5, -34.5, 70.58)
--      )
--
    initialBilinearForms = BilinearForms 1e-32 [(Nothing, bilinearMat)]
    mLambda :: Maybe (V 4 Rational)
    mLambda = Just $ toV (0.3501, 0.165, 0.6787, 1) --(-0.258492, -0.123751, -0.515709, -0.807397) --(1,1,1,1)
    bound deltaExts = Bound
      { boundKey = gnySigPsiEpsFeasibleDefaultGaps deltaExts mLambda ngroup nmax
      , precision = B3d.precision (Params.block3dParamsNmax nmax)
      , solverParams = (Params.jumpFindingParams nmax) { precision = 768 }
      , boundConfig = defaultBoundConfig
      }

boundsProgram "GNYSigPsiEps_OPESearchTest_ngroup2_nmax8" =
  local (setJobType (MPIJob 1 18) . setJobTime (8*hour)) $ void $ do
  checkpointMap <- Bound.newCheckpointMap affine deltaExtToV Nothing
  lambdaMap     <- Bound.newLambdaMap affine deltaExtToV Nothing
  mapConcurrently_ (remoteGNYSigPsiEpsOPESearch checkpointMap lambdaMap initialBilinearForms . bound) $
     fmap toV [(1.0686, 0.651, 1.70)] --n2nmax6PrimalPoints --GNYData.epsN2nmax6DualPoints --[(1.06 + x/200, 0.6 + y/50, 1.3 + z/10) | x <-[0..4], y <- [0..5], z <- [0..7]]
  where
    nmax = 8
    ngroup = 2
    affine = GNYData.gny2Affine3dWindow--gny2Affine3dNmax6
    bilinearMat :: Matrix 4 4 Rational
    bilinearMat = toM
     ( (30181.9, 7654.11, -18389.5, 851.26)
     , (7654.11, 18817.2, -8700.25, 230.935)
     , (-18389.5, -8700.25, 12237.9, -567.21)
     , (851.259, 230.935, -567.21, 52.34)
     )
    initialBilinearForms = BilinearForms 1e-32 [(Nothing, bilinearMat)]
    mLambda :: Maybe (V 4 Rational)
    mLambda = Just $ toV (-0.258492, -0.123751, -0.515709, -0.807397) --(1,1,1,1)
    bound deltaExts = Bound
      { boundKey = gnySigPsiEpsFeasibleDefaultGaps deltaExts mLambda ngroup nmax
      , precision = B3d.precision (Params.block3dParamsNmax nmax)
      , solverParams = (Params.jumpFindingParams nmax) { precision = 768 }
      , boundConfig = defaultBoundConfig
      }


boundsProgram "GNYSigPsiEps_OPESearchTest_ngroup2_nmax14" =
  local (setJobType (MPIJob 1 128) . setJobTime (24*hour) . setJobMemory "250G" . setSlurmPartition "compute") $ void $ do
  checkpointMap <- Bound.newCheckpointMap affine deltaExtToV Nothing
  lambdaMap     <- Bound.newLambdaMap affine deltaExtToV Nothing
  mapConcurrently_ (remoteGNYSigPsiEpsOPESearch checkpointMap lambdaMap initialBilinearForms . bound) $
     fmap toV GNYData.opeSig25N2nmax14PrimalPoints
  where
    nmax = 14
    ngroup = 2
    affine = GNYData.gny2OpeSig25AffineNmax12
    initialBilinearForms = BilinearForms 1e-32 [(Nothing, GNYData.gny2Sig25bilinearMatNmax12)]
    mLambda :: Maybe (V 4 Rational)
    mLambda = Just $ toV (-0.258492, -0.123751, -0.515709, -0.807397) --(1,1,1,1)
    bound deltaExts = Bound
      { boundKey = gnySigPsiEpsFeasibleDefaultGaps deltaExts mLambda ngroup nmax
      , precision = B3d.precision (Params.block3dParamsNmax nmax)
      , solverParams = (Params.jumpFindingParams nmax) { precision = 768
                                                , procGranularity = 2 }
      , boundConfig = defaultBoundConfig
      }

boundsProgram "GNYSigPsiEps_OPESearchTest_ngroup2_nmax18" =
  local (setJobType (MPIJob 4 128) . setJobTime (48*hour) . setJobMemory "0G" . setSlurmPartition "compute") $ void $ do
  checkpointMap <- Bound.newCheckpointMap affine deltaExtToV Nothing
  lambdaMap     <- Bound.newLambdaMap affine deltaExtToV mLambda --Nothing
  mapConcurrently_ (remoteGNYSigPsiEpsOPESearch checkpointMap lambdaMap initialBilinearForms . bound) $
     fmap toV [(1.0679508596642033, 0.653003850548663, 1.7333812567657645)] --GNYData.opeSig25N2nmax18PrimalPoints
  where
    nmax = 18
    ngroup = 2
    affine = GNYData.gny2OpeSig25AffineNmax12
    initialBilinearForms = BilinearForms 1e-32 [(Nothing, GNYData.gny2Sig25bilinearMatNmax12)]
    mLambda :: Maybe (V 4 Rational)
    mLambda = Just $ toV (0.310566, 0.14493, 0.611389, 1)
    bound deltaExts = Bound
      { boundKey = gnySigPsiEpsFeasibleDefaultGaps deltaExts mLambda ngroup nmax
      , precision = B3d.precision (Params.block3dParamsNmax nmax)
      , solverParams = (Params.jumpFindingParams nmax) { precision = 768
                                                , procGranularity = 32 }
      , boundConfig = defaultBoundConfig
      }


boundsProgram "GNYSigPsiEps_delaunay_OPEScan_ngroup2_nmax6" =
  gnyDelaunaySearchOPEScan ngroup searchData
  where
    ngroup = 2
--    bilinearMat :: Matrix 4 4 Rational
--    bilinearMat = toM
--     ( (2226.2016, -2181.1580, -693.6937, 53.1024)
--     , (-2181.1580, 4852.3925, -20.5093, -28.4479)
--     , (-693.6937, - 20.5093, 413.5043, -33.8579)
--     , (53.1024, -28.4479, -33.8579, 7.4766)
--     )
--     ( (245, 0, 0, -77)
--     , (0, 245, 0, -47.25)
--     , (0, 0, 245, -162.75)
--     , (-77, -47.25, -162.75, 133.08)
--     )
    lambda :: V 4 Rational
    lambda = toV (-0.258492, -0.123751, -0.515709, -0.807397)
    searchData = GNYDelaunaySearchData
        { nmax = 6
        , affine = GNYData.gny2OpeSig25AffineNmax6
        , initialLambda = lambda
        , initialCheckpoint = Nothing
        , initialDisallowed = fmap toV $ GNYData.opeSig25N2nmax6DualPoints
        , initialAllowed = fmap toV $ GNYData.opeSig25N2nmax6PrimalPoints
        , opeEllipse = GNYData.gny2Sig25bilinearMatNmax6
        , solverPrecision = 768
        , delaunayConfig = defaultDelaunayConfig 15 300
        , jobTime = 8*hour
        , jobType = MPIJob 1 16
        , jobMemory = "90G"
        }

boundsProgram "GNYSigPsiEps_delaunay_OPEScan_ngroup2_nmax8" =
  gnyDelaunaySearchOPEScan ngroup searchData
  where
    ngroup = 2
    lambda :: V 4 Rational
    lambda = toV (-0.258492, -0.123751, -0.515709, -0.807397)
    searchData = GNYDelaunaySearchData
        { nmax = 8
        , affine = GNYData.gny2OpeSig25AffineNmax6
        , initialLambda = lambda
        , initialCheckpoint = Nothing
        , initialDisallowed = fmap toV $ GNYData.opeSig25N2nmax8DualPoints
        , initialAllowed = fmap toV $ GNYData.opeSig25N2nmax8PrimalPoints
        , opeEllipse = GNYData.gny2Sig25bilinearMatNmax6
        , solverPrecision = 768
        , delaunayConfig = defaultDelaunayConfig 15 300
        , jobTime = 16*hour
        , jobType = MPIJob 2 32
        , jobMemory = "180G"
        }

boundsProgram "GNYSigPsiEps_delaunay_OPEScan_ngroup2_nmax12" =
  gnyDelaunaySearchOPEScan ngroup searchData
  where
    ngroup = 2
    lambda :: V 4 Rational
    lambda = toV (-0.258492, -0.123751, -0.515709, -0.807397)
    searchData = GNYDelaunaySearchData
        { nmax = 12
        , affine = GNYData.gny2OpeSig25AffineNmax6
        , initialLambda = lambda
        , initialCheckpoint = Nothing
        , initialDisallowed = fmap toV $ GNYData.opeSig25N2nmax10DualPoints
        , initialAllowed = fmap toV $ GNYData.opeSig25N2nmax12PrimalPoints
        , opeEllipse = GNYData.gny2Sig25bilinearMatNmax6
        , solverPrecision = 768
        , delaunayConfig = defaultDelaunayConfig 15 300
        , jobTime = 16*hour
        , jobType = MPIJob 4 32
        , jobMemory = "180G"
        }

boundsProgram "GNYSigPsiEps_delaunay_OPEScan_ngroup2_nmax14" =
  gnyDelaunaySearchOPEScan ngroup searchData
  where
    ngroup = 2
    lambda :: V 4 Rational
    lambda = toV (-0.258492, -0.123751, -0.515709, -0.807397)
    searchData = GNYDelaunaySearchData
        { nmax = 14
        , affine = GNYData.gny2OpeSig25AffineNmax12
        , initialLambda = lambda
        , initialCheckpoint = Nothing
        , initialDisallowed = fmap toV $ [(4088655640268475617/3840000000000000000, 856480427967628843/ 1280000000000000000, 4558819913255358761/ 2560000000000000000)] --GNYData.opeSig25N2nmax14DualPoints
        , initialAllowed = fmap toV $ [(384574102569100321/360000000000000000, 26031679066996177/ 40000000000000000, 15497460640305691/ 9000000000000000)]--GNYData.opeSig25N2nmax14PrimalPoints
        , opeEllipse = GNYData.gny2Sig25bilinearMatNmax12
        , solverPrecision = 768
        , delaunayConfig = defaultDelaunayConfig 4 100
        , jobTime = 24*hour
        , jobType = MPIJob 4 64
        , jobMemory = "0"
        }

boundsProgram "GNYSigPsiEps_delaunay_OPEScan_ngroup2_nmax18" =
  gnyDelaunaySearchOPEScan ngroup searchData
  where
    ngroup = 2
    lambda :: V 4 Rational
    lambda = toV (0.310566, 0.14493, 0.611389, 1)
    searchData = GNYDelaunaySearchData
        { nmax = 18
        , affine = GNYData.gny2OpeSig25AffineNmax12
        , initialLambda = lambda
        , initialCheckpoint = Just "/expanse/lustre/scratch/aikeliu/temp_project/data/2021-09/OWxSH/Object_LCJdpahFqKpEDRfIoIyoJcv_ZBzeA1WhlVnXq_LP5kQ/ck"
        , initialDisallowed = fmap toV $ GNYData.opeSig25N2nmax14DualPoints
        , initialAllowed = fmap toV $ [(1.0679508596642033, 0.653003850548663, 1.7333812567657645)]
        , opeEllipse = GNYData.gny2Sig25bilinearMatNmax12
        , solverPrecision = 768
        , delaunayConfig = defaultDelaunayConfig 2 100
        , jobTime = 48*hour
        , jobType = MPIJob 4 128
        , jobMemory = "0G"
        }



boundsProgram "GNYSigPsiEps_OPESearchTest_ngroup2_nmax4" =
  local (setJobType (MPIJob 1 9) . setJobTime (8*hour)) $ void $ do
  checkpointMap <- Bound.newCheckpointMap affine deltaExtToV Nothing
  lambdaMap     <- Bound.newLambdaMap affine deltaExtToV Nothing
  mapConcurrently_ (remoteGNYSigPsiEpsOPESearch checkpointMap lambdaMap initialBilinearForms . bound) $
     fmap toV [(1.069,0.65,1.66)]--[(1.069,0.65,1.7)] --n2nmax6PrimalPoints --GNYData.epsN2nmax6DualPoints --[(1.06 + x/200, 0.6 + y/50, 1.3 + z/10) | x <-[0..4], y <- [0..5], z <- [0..7]]
  where
    nmax = 4
    ngroup = 2
    affine = GNYData.gny2Affine3dNmax6
    bilinearMat :: Matrix 4 4 Rational
    bilinearMat = toM
      ( (1,0,0,0)
      , (0,1,0,0)
      , (0,0,-100,0)
      , (0,0,0,1)
      )
    initialBilinearForms = BilinearForms 1e-32 [(Nothing, bilinearMat)]
    mLambda :: Maybe (V 4 Rational)
    mLambda = Just $ toV (0.5,0.222,1,1.65)
    bound deltaExts = Bound
      { boundKey = gnyLooseFeasibleDefaultGaps deltaExts mLambda ngroup nmax
      , precision = B3d.precision (Params.block3dParamsNmax nmax)
      , solverParams = (Params.jumpFindingParams nmax) { precision = 768 }
      , boundConfig = defaultBoundConfig
      }

boundsProgram "GNYSigPsiEp_island_ngroup2_nmax6_sanity_check" =
  local (setJobType (MPIJob 2 32) . setJobTime (8*hour)) $ do
  checkpointMap <- Bound.newCheckpointMap affine deltaExtToV mInitialCheckpoint
  let
    f :: V 3 Rational -> Cluster Bool
    f deltaV = fmap SDPB.isPrimalFeasible $
      Bound.remoteComputeWithCheckpointMap checkpointMap $
      bound deltaV
  mapConcurrently_ f [ toV (1.06869, 0.647902, 1.66213) -- should be primal
                     , toV (1.06634, 0.637824, 1.61584) -- should be dual
                     ]
  where
    nmax = 6
    ngroup = 2
    affine = GNYData.gny2Affine3dWindow
    bound deltaV = Bound
      { boundKey = gnySigPsiEpsFeasibleDefaultGaps deltaV Nothing ngroup nmax
      , precision = B3d.precision (Params.block3dParamsNmax nmax)
      , solverParams = (Params.jumpFindingParams nmax)
        { precision = 768
        , dualErrorThreshold = 1e-30
        , primalErrorThreshold = 1e-40
        , findPrimalFeasible = False
        }
      , boundConfig = defaultBoundConfig
      }
    mInitialCheckpoint = Nothing

boundsProgram "GNYSigPsiEps_OPESearchTest_ngroup2_nmax4_DSD" =
  local (setJobType (MPIJob 2 32) . setJobTime (8*hour)) $ do
  checkpointMap <- Bound.newCheckpointMap affine deltaExtToV Nothing
  lambdaMap     <- Bound.newLambdaMap affine deltaExtToV mLambda
  mapConcurrently_ (remoteGNYSigPsiEpsOPESearch checkpointMap lambdaMap initialBilinearForms . bound . toV) $
    --[ toV (1.06869, 0.647902, 1.66213) ]
    [ (1.068781493245072, 0.6476503287755234, 1.6323210786005182)
    , (1.069428407008492, 0.6457815396143913, 1.656708119766324)
    , (1.069078151372765, 0.6371140829150129, 1.621876597794288)
    , (1.0691552304168275, 0.6441161259709939, 1.7025062412875678)
    , (1.0698269601659725, 0.6512442548263832, 1.6756578365340429)
    , (1.069585642384212, 0.6470206483951152, 1.6560841736730794)
    , (1.0678605156362746, 0.6424587973776331, 1.6241625937230806)
    , (1.069071769142553, 0.6531816902562142, 1.6978914457551222)
    , (1.0687063759600175, 0.657042107325164, 1.6952882145715387)
    , (1.070160110695107, 0.6502920013162125, 1.7136246678050426)
    , (1.0686127344428658, 0.6534266597066488, 1.6818250500296827)
    , (1.0672994294831373, 0.6412438732321775, 1.639908037769334)
    , (1.0677935855321705, 0.644963790594179, 1.674745998077241)
    , (1.0698784175027132, 0.6554511087755771, 1.7063599464565602)
    , (1.0675819891432228, 0.6376834377237421, 1.615309505709341)
    , (1.068179202881803, 0.6517555085791709, 1.640742187579863)
    , (1.0692007505946934, 0.6483866924056343, 1.668732508955958)
    , (1.0679593410729733, 0.649780134328507, 1.654698422161644)
    , (1.0680311131154334, 0.6541925462281628, 1.6705491674574948)
    , (1.0689758617959666, 0.641746240111719, 1.674566349541181)
    ]
  where
    nmax = 4
    ngroup = 2
    affine = GNYData.gny2Affine3dNmax6
    opeEllipse :: Matrix 4 4 Rational
    opeEllipse = toM
      ( (0.601498, 0, 0.309772, 0)
      , (0, 0.601498, 0.147367, 0)
      , (0.309772, 0.147367, 0.202489, -0.00444574)
      , (0, 0, -0.00444574, 0.00267333)
      )
      -- ( (0.601498, 0, -0.309772, 0)
      -- , (0, 0.601498, 0.147367, 0)
      -- , (-0.309772, 0.147367, 0.202489, -0.00444574)
      -- , (0, 0, -0.00444574, 0.00267333)
      -- )
    initialBilinearForms = BilinearForms 1e-32 [(Nothing, opeEllipse)]
    mLambda :: Maybe (V 4 Rational)
    mLambda = Just $ toV (-0.515, -0.245, 1, 1.663)
    bound deltaExts = Bound
      { --boundKey = gnyLooseFeasibleDefaultGaps deltaExts mLambda ngroup nmax
        boundKey = gnySigPsiEpsFeasibleDefaultGaps deltaExts mLambda ngroup nmax
      , precision = B3d.precision (Params.block3dParamsNmax nmax)
      , solverParams = (Params.jumpFindingParams nmax)
        { precision = 768
        , dualErrorThreshold = 1e-80
        , findDualFeasible = True
        }
      , boundConfig = defaultBoundConfig
      }

--------
--GFF --
--------
boundsProgram "GNYSigPsiEps_GFF_test_ngroup2_nmax6" =
  local (setJobType (MPIJob 1 32) . setJobTime (24*hour)) $ void $
  mapConcurrently_ (remoteComputeGNYSigPsiEpsBounds . map bound) $ chunksOf 50 $ do
  (deltaPsi, deltaSig, deltaEps) <- [(1.06644, 0.657704, 1.75543)]
  --deltaPsi <- [1.055, 1.060 .. 1.075]
  --deltaSig <- [0.69, 0.71 .. 0.75]-- .. 0.651]
  --deltaEps <- [1.75]-- .. 1.725]
  pure (deltaPsi, deltaSig, deltaEps)
  where
    nmax = 6
    ngroup = 2
    --deltaPsi = 1.0686
    mLambda :: Maybe (V 4 Rational)
    mLambda = Just $ toV (-0.254569, -0.11829, -0.501378, -0.818389)
    bound (deltaPsi, deltaSig, deltaEps)  = Bound
      { boundKey = gnySigPsiEpsGFFNavigatorDefaultGaps (toV (deltaPsi, deltaSig, deltaEps)) mLambda ngroup nmax
      , precision = B3d.precision (Params.block3dParamsNmax nmax)
      , solverParams = (Params.optimizationParams nmax) { precision = 768}
                                             --, dualErrorThreshold=1e-30
                                             --, primalErrorThreshold=1e-40}
                                             --, findPrimalFeasible = False }
      , boundConfig = defaultBoundConfig
      }


boundsProgram "GNYSigPsiEps_GFFNavigatorBFGS_ngroup2_nmax6" =
  local (setJobType (MPIJob 1 32) . setJobTime (48*hour) . setJobMemory "90G" . setSlurmPartition "shared") $
  do
    result :: (V 3 Rational, [BFGS.BFGSData n (Rounded 'TowardZero 200)]) <-
      SDPDeriv.remoteBFGSBound bfgsConfig bjConfig
    Log.info "Finished BFGS search" result
  where
    bbMin = toV (1.060, 0.64, 1.4)
    bbMax = toV (1.070, 0.70, 1.9)
    bfgsConfig = BFGS.MkConfig
      { BFGS.epsGradHess         = 1e-9
      , BFGS.stepResolution      = 1e-32
      , BFGS.gradNormThreshold   = 1e-7
      , BFGS.stopOnNegative      = True
      , BFGS.boundingBoxMin      = bbMin
      , BFGS.boundingBoxMax      = bbMax
      , BFGS.initialHessianGuess = Nothing
      }
    bjConfig = SDPDeriv.GetBoundJetConfig
      { centeringIterations = 10
      , fileTreatment = Bound.keepOutAndCheckpoint
      , boundClosure = cPtr (static mkBound)
      , valFromObjClosure = cPtr (static (SDPDeriv.MkFractionalMap (\y -> y/(1-y))))
      , initialPoint = toV (1.06644, 0.657704, 1.75543)
      , checkpointMapName = Nothing
      }
    mkBound :: V 3 Rational -> Bound Int GNYSigPsiEps.GNYSigPsiEps
    mkBound v = Bound
      { boundKey = gnySigPsiEpsGFFNavigatorDefaultGaps v (Just lambdaInit) ngroup nmax
      , precision = B3d.precision (Params.block3dParamsNmax nmax)
      , solverParams = (Params.optimizationParams nmax)
                       { precision = 768
                       , dualityGapThreshold = 1e-30
                       }
      , boundConfig = defaultBoundConfig
      }
      where -- this is so static stuff works correctly
        ngroup = 2
        nmax = 6
        lambdaInit = toV (-0.254569, -0.11829, -0.501378, -0.818389)


boundsProgram "GNYSigPsiEps_GFFNavigatorBFGS_ngroup2_nmax10" =
  local (setJobType (MPIJob 1 32) . setJobTime (24*hour) . setJobMemory "90G") $ do
    result :: ([V 3 Rational], BFGS.BFGSData n (Rounded 'TowardZero 200)) <-
      SDPDeriv.remoteBFGSExtremize islandExtConfig bjConfig extremaDirs
    Log.info "Finished BFGS search" result
--    result :: (V 2 Rational, [BFGS.BFGSData n (Rounded 'TowardZero 200)]) <-
--      SDPDeriv.remoteBFGSBound bfgsConfig bjConfig "onVecCheckpoints"
--    Log.info "Finished BFGS search" result
  where
    extremaDirs = [toV (1,0,0), toV (-1,0,0), toV (0,1,0), toV (0,-1,0), toV (0,0,1), toV (0,0,-1)]
    bbMin = toV (1.066, 0.63, 1.3)
    bbMax = toV (1.072, 0.68, 1.8)
    bfgsConfig' = BFGS.MkConfig
      { BFGS.epsGradHess         = 1e-9
      , BFGS.stepResolution      = 1e-32
      , BFGS.gradNormThreshold   = 1e-7
      , BFGS.stopOnNegative      = True
      , BFGS.boundingBoxMin      = bbMin
      , BFGS.boundingBoxMax      = bbMax
      , BFGS.initialHessianGuess = Nothing
      }
    islandExtConfig = BFGS.MkIslandExtConfig
      { bfgsConfig         = bfgsConfig'
      , goalToleranceObj   = 1e-4
      , goalToleranceGrad  = 1e-4
      , lineSearchDecrease = 0.8
      , maxIters           = Nothing
      }
    bjConfig = SDPDeriv.GetBoundJetConfig
      { centeringIterations = 10
      , fileTreatment = Bound.keepOutAndCheckpoint
      , boundClosure = cPtr (static mkBound)
      , valFromObjClosure = cPtr (static (SDPDeriv.MkFractionalMap (\y -> y/(1-y))))
      , initialPoint = toV (1.06644, 0.657704, 1.75543)
      , checkpointMapName = Nothing
      }
    mkBound :: V 3 Rational -> Bound Int GNYSigPsiEps.GNYSigPsiEps
    mkBound v = Bound
      { boundKey = gnySigPsiEpsGFFNavigatorDefaultGaps v (Just lambdaInit) ngroup nmax
      , precision = B3d.precision (Params.block3dParamsNmax nmax)
      , solverParams = (Params.optimizationParams nmax)
                       { precision = 768
                       , dualityGapThreshold = 1e-30
                       }
      , boundConfig = defaultBoundConfig
      }
      where -- this is so static stuff works correctly
        ngroup = 2
        nmax = 10
        lambdaInit = toV (-0.254569, -0.11829, -0.501378, -0.818389)


boundsProgram "GNYSigPsiEpsSigP_GFFNavigatorBFGS_ngroup2_nmax6" =
  local (setJobType (MPIJob 1 32) . setJobTime (48*hour) . setJobMemory "90G" . setSlurmPartition "shared") $
  do
    result :: (V 4 Rational, [BFGS.BFGSData n (Rounded 'TowardZero 200)]) <-
      SDPDeriv.remoteBFGSBound bfgsConfig bjConfig
    Log.info "Finished BFGS search" result
  where
    bbMin = toV (1.060, 0.64, 1.4, 2.8)
    bbMax = toV (1.070, 0.70, 1.9, 3.2)
    bfgsConfig = BFGS.MkConfig
      { BFGS.epsGradHess         = 1e-9
      , BFGS.stepResolution      = 1e-32
      , BFGS.gradNormThreshold   = 1e-7
      , BFGS.stopOnNegative      = True
      , BFGS.boundingBoxMin      = bbMin
      , BFGS.boundingBoxMax      = bbMax
      , BFGS.initialHessianGuess = Nothing
      }
    bjConfig = SDPDeriv.GetBoundJetConfig
      { centeringIterations = 10
      , fileTreatment = Bound.keepOutAndCheckpoint
      , boundClosure = cPtr (static mkBound)
      , valFromObjClosure = cPtr (static (SDPDeriv.MkFractionalMap (\y -> y/(1-y))))
      , initialPoint = toV (1.06644, 0.657704, 1.75543, 3)
      , checkpointMapName = Nothing
      }
    mkBound :: V 4 Rational -> Bound Int GNYSigPsiEps.GNYSigPsiEps
    mkBound v = Bound
      { boundKey = gnySigPsiEpsSigPGFFNavigatorDefaultGaps v (Just lambdaInit) ngroup nmax
      , precision = B3d.precision (Params.block3dParamsNmax nmax)
      , solverParams = (Params.optimizationParams nmax)
                       { precision = 768
                       , dualityGapThreshold = 1e-30
                       }
      , boundConfig = defaultBoundConfig
      }
      where -- this is so static stuff works correctly
        ngroup = 2
        nmax = 6
        lambdaInit = toV (-0.254569, -0.11829, -0.501378, -0.818389)

boundsProgram "GNYSigPsiEpsSigP_GFFNavigatorBFGS_ngroup2_nmax18" =
  local (setJobType (MPIJob 4 128) . setJobTime (48*hour) . setJobMemory "0G" . setSlurmPartition "compute") $
  do
    result :: (V 4 Rational, [BFGS.BFGSData n (Rounded 'TowardZero 200)]) <-
      SDPDeriv.remoteBFGSBound bfgsConfig bjConfig
    Log.info "Finished BFGS search" result
  where
    bbMin = toV (1.065, 0.645, 1.7, 2.8)
    bbMax = toV (1.070, 0.67, 1.77, 3.2)
    bfgsConfig = BFGS.MkConfig
      { BFGS.epsGradHess         = 1e-9
      , BFGS.stepResolution      = 1e-32
      , BFGS.gradNormThreshold   = 1e-7
      , BFGS.stopOnNegative      = True
      , BFGS.boundingBoxMin      = bbMin
      , BFGS.boundingBoxMax      = bbMax
      , BFGS.initialHessianGuess = Nothing
      }
    bjConfig = SDPDeriv.GetBoundJetConfig
      { centeringIterations = 10
      , fileTreatment = Bound.keepOutAndCheckpoint
      , boundClosure = cPtr (static mkBound)
      , valFromObjClosure = cPtr (static (SDPDeriv.MkFractionalMap (\y -> y/(1-y))))
      , initialPoint = toV (1.0679508596642033, 0.653003850548663, 1.7333812567657645, 3)
      , checkpointMapName = Nothing
      }
    mkBound :: V 4 Rational -> Bound Int GNYSigPsiEps.GNYSigPsiEps
    mkBound v = Bound
      { boundKey = gnySigPsiEpsSigPGFFNavigatorDefaultGaps v (Just lambdaInit) ngroup nmax
      , precision = B3d.precision (Params.block3dParamsNmax nmax)
      , solverParams = (Params.optimizationParams nmax)
                       { precision = 768
                       , dualityGapThreshold = 1e-30
                       , procGranularity = 32
                       }
      , boundConfig = defaultBoundConfig
      }
      where -- this is so static stuff works correctly
        ngroup = 2
        nmax = 18
        lambdaInit = toV (0.310566, 0.14493, 0.611389, 1)


-----------
--- N=4 ---
-----------

boundsProgram "GNYSigPsiEps_island_ngroup4_nmax4" =
  gnyDelaunaySearchOPEScan ngroup searchData
  where
    ngroup = 4
    lambda :: V 4 Rational
    lambda = toV ( (-34901313) / 131857384
                            , 8876292 / 92371273
                            , 54596795 / 92506868
                            , 126143911 / 166781346)
    searchData = GNYDelaunaySearchData
        { nmax = 4
        , affine = AffineTransform { affineShift  = toV (1.05, 0.75, 1.75)
                                   , affineLinear = toV ( toV (0.05, 0, 0)
                                                        , toV (0,  0.25, 0)
                                                        , toV (0,0,0.75)  )
                                   }
        , initialLambda = lambda
        , initialCheckpoint = Nothing
        , initialDisallowed = []
        , initialAllowed = [ toV (1.044,0.77,1.8)]
        , opeEllipse = toM ( (1,0,0,0)
                           , (0,1.4,0,0)
                           , (0,0,-1,0)
                           , (0,0,0,1/100))
        , solverPrecision = 768
        , delaunayConfig = defaultDelaunayConfig 32 400
        , jobTime = 8*hour
        , jobType = MPIJob 1 9
        , jobMemory = "50G"
        }

boundsProgram "GNYSigPsiEps_OPESearchTest_ngroup4_nmax2" =
  local (setJobType (MPIJob 1 9) . setJobTime (8*hour) . setSlurmQos "nothrottle") $ void $ do
  checkpointMap <- Bound.newCheckpointMap affine deltaExtToV Nothing
  lambdaMap     <- Bound.newLambdaMap affine deltaExtToV Nothing
  mapConcurrently_ (remoteGNYSigPsiEpsOPESearch checkpointMap lambdaMap initialBilinearForms . bound) $
    fmap toV [(1.044+x,0.751981+y,1.75945+z) | x <- [-0.004,0,0.004], y <- [-0.02,0,0.02], z <- [-0.02,0,0.02]] --N4.gny4AllowedPts
  where
    nmax = 2
    ngroup = 4
    affine = GNYData.gny2Affine3dWindow--gny2Affine3dNmax6
    bilinearMat :: Matrix 4 4 Rational
    bilinearMat = toM ((1,0,0,0)
                      ,(0,1,0,0)
                      ,(0,0,-100,0)
                      ,(0,0,0,1))
                      -- ( (20.0, 0, 0, -8.0)
                      -- , (0, 20.0, 0, -3.5)
                      -- , (0, 0, 20, -14.5)
                      -- , (-8.0, -3.5, -14.5, 5.98)
                      -- )
    initialBilinearForms = BilinearForms 1e-32 [(Nothing, bilinearMat)]
    mLambda :: Maybe (V 4 Rational)
    mLambda = Just $ toV ( (-34901313) / 131857384
                            , 8876292 / 92371273
                            , 54596795 / 92506868
                            , 126143911 / 166781346)--(1,1,1,1)
    bound deltaExts = Bound
      { boundKey = gnySigPsiEpsFeasibleDefaultGaps deltaExts mLambda ngroup nmax
      , precision = B3d.precision (Params.block3dParamsNmax nmax)
      , solverParams = (Params.jumpFindingParams nmax) { precision = 768 }
      , boundConfig = defaultBoundConfig
      }

boundsProgram "GNYSigPsiEps_delaunay_OPEScan_ngroup4_nmax2" =
  gnyDelaunaySearchOPEScan ngroup searchData
  where
    ngroup = 4
    lambda :: V 4 Rational
    lambda = toV (-0.258492, -0.123751, -0.515709, -0.807397)
    searchData = GNYDelaunaySearchData
        { nmax = 2
        , affine = N4.nmax2affine3d
        , initialLambda = lambda
        , initialCheckpoint = Nothing
        , initialDisallowed = []
        , initialAllowed = []
        , opeEllipse = toM
                       ( (1,0,0,0)
                       , (0,1,0,0)
                       , (0,0,-100,0)
                       , (0,0,0,1)
                       )
        , solverPrecision = 768
        , delaunayConfig = defaultDelaunayConfig 16 600
        , jobTime = 8*hour
        , jobType = MPIJob 1 9
        , jobMemory = "30G"
        }

boundsProgram "GNYSigPsiEps_delaunay_OPEScan_ngroup4_nmax4" =
  gnyDelaunaySearchOPEScan ngroup searchData
  where
    ngroup = 4
    lambda :: V 4 Rational
    lambda = toV (0.43984004785635333, 0.1499339661556141,1, 1.803900477723386)
    searchData = GNYDelaunaySearchData
        { nmax = 4
        , affine = N4.nmax4Affine
        , initialLambda = lambda
        , initialCheckpoint = Nothing
        , initialDisallowed = []
        , initialAllowed = []
        , opeEllipse = N4.nmax6opeEllipse
        , solverPrecision = 768
        , delaunayConfig = defaultDelaunayConfig 8 600
        , jobTime = 8*hour
        , jobType = MPIJob 1 24
        , jobMemory = "30G"
        }



boundsProgram "GNYSigPsiEps_delaunay_OPEScan_ngroup4_nmax10" =
  gnyDelaunaySearchOPEScan ngroup searchData
  where
    ngroup = 4
    lambda :: V 4 Rational
    lambda = toV (0.43984004785635333, 0.1499339661556141,1, 1.803900477723386)
    searchData = GNYDelaunaySearchData
        { nmax = 10
        , affine = N4.nmax6affine3d
        , initialLambda = lambda
        , initialCheckpoint = Nothing
        , initialDisallowed = []
        , initialAllowed = []
        , opeEllipse = N4.nmax6opeEllipse
        , solverPrecision = 768
        , delaunayConfig = defaultDelaunayConfig 8 600
        , jobTime = 24*hour
        , jobType = MPIJob 4 24
        , jobMemory = "100G"
        }

boundsProgram "GNYSigPsiEps_OPESearchGrid_ngroup4_nmax6" =
  local (setJobType (MPIJob 1 4) . setJobTime (8*hour) . setSlurmPartition "pi_poland,day,scavenge") $ void $ do
  checkpointMap <- Bound.newCheckpointMap affine deltaExtToV Nothing
  lambdaMap     <- Bound.newLambdaMap affine deltaExtToV Nothing
  mapConcurrently_ (remoteGNYSigPsiEpsOPESearch checkpointMap lambdaMap initialBilinearForms . bound) $ fmap (apply affine) $
     fmap toV [(x,y,z) | x <- [-1,-0.5..1], y <- [-1,-0.5..1], z <- [-1,-0.5..1]] --n2nmax6PrimalPoints --GNYData.epsN2nmax6DualPoints --[(1.06 + x/200, 0.6 + y/50, 1.3 + z/10) | x <-[0..4], y <- [0..5], z <- [0..7]]
  where
    nmax = 6
    ngroup = 4
    affine = N4.nmax6affine3d--gny2Affine3dNmax6
    bilinearMat :: Matrix 4 4 Rational
    bilinearMat = toM
     ((-1,0,0,0)
     ,(0,-1,0,0)
     ,(0,0,-1,0)
     ,(0,0,0,0.01))
--
    initialBilinearForms = BilinearForms 1e-32 [(Nothing, bilinearMat)]
    mLambda :: Maybe (V 4 Rational)
    mLambda = Just $ toV (0.3501, 0.165, 0.6787, 1) --(-0.258492, -0.123751, -0.515709, -0.807397) --(1,1,1,1)
    bound deltaExts = Bound
      { boundKey = gnySigPsiEpsFeasibleDefaultGaps deltaExts mLambda ngroup nmax
      , precision = B3d.precision (Params.block3dParamsNmax nmax)
      , solverParams = (Params.jumpFindingParams nmax) { precision = 768 }
      , boundConfig = defaultBoundConfig
      }

boundsProgram "GNYSigPsiEps_OPESearchTest_ngroup4_nmax4" =
  local (setJobType (MPIJob 1 24) . setJobTime (8*hour) . setJobMemory "100G" . setSlurmPartition "compute") $ void $ do
  checkpointMap <- Bound.newCheckpointMap affine deltaExtToV Nothing
  lambdaMap     <- Bound.newLambdaMap affine deltaExtToV Nothing
  mapConcurrently_ (remoteGNYSigPsiEpsOPESearch checkpointMap lambdaMap initialBilinearForms . bound) $
    fmap (apply affine . toV) [(x,y,z) | x <- [-0.75,-0.25..0.75], y <- [-0.75,-0.25..0.75], z <- [-0.75,-0.25..0.75] ] --N4.gny4AllowedPts
  where
    nmax = 4
    ngroup = 4
    affine = N4.nmax4affine3d--gny2Affine3dNmax6
    bilinearMat :: Matrix 4 4 Rational
    bilinearMat = toM
     ((-1,0,0,0)
     ,(0,-1,0,0)
     ,(0,0,-1,0)
     ,(0,0,0,0.01))
    initialBilinearForms = BilinearForms 1e-32 [(Nothing, bilinearMat)]
    mLambda :: Maybe (V 4 Rational)
    mLambda = Just $ toV (0.43984004785635333, 0.1499339661556141,1, 1.803900477723386)--(1,1,1,1)
    bound deltaExts = Bound
      { boundKey = gnySigPsiEpsFeasibleDefaultGaps deltaExts mLambda ngroup nmax
      , precision = B3d.precision (Params.block3dParamsNmax nmax)
      , solverParams = (Params.jumpFindingParams nmax) { precision = 768 }
      , boundConfig = defaultBoundConfig
      }

boundsProgram "GNYSigPsiEps_OPESearchNarrowGrid_ngroup4_nmax6" =
  local (setJobType (MPIJob 1 9) . setJobTime (24*hour) . setJobMemory "45G" . setSlurmQos "nothrottle" . setSlurmPartition "pi_poland,day,scavenge") $ void $ do
  checkpointMap <- Bound.newCheckpointMap affine deltaExtToV Nothing
  lambdaMap     <- Bound.newLambdaMap affine deltaExtToV Nothing
  mapConcurrently_ (remoteGNYSigPsiEpsOPESearch checkpointMap lambdaMap initialBilinearForms . bound) $
    fmap (apply affine . toV) [(x,y,z) | x <- [-0.375,-0.25..0.375], y <- [0.375,0.5..0.875], z <- [-0.375,-0.25..0.375] ] --N4.gny4AllowedPts
  where
    nmax = 6
    ngroup = 4
    affine = N4.nmax4affine3d--gny2Affine3dNmax6
    bilinearMat :: Matrix 4 4 Rational
    bilinearMat = toM
     ((-1,0,0,0)
     ,(0,-1,0,0)
     ,(0,0,-1,0)
     ,(0,0,0,0.01))
    initialBilinearForms = BilinearForms 1e-32 [(Nothing, bilinearMat)]
    mLambda :: Maybe (V 4 Rational)
    mLambda = Just $ toV (0.43984004785635333, 0.1499339661556141,1, 1.803900477723386)--(1,1,1,1)
    bound deltaExts = Bound
      { boundKey = gnySigPsiEpsFeasibleDefaultGaps deltaExts mLambda ngroup nmax
      , precision = B3d.precision (Params.block3dParamsNmax nmax)
      , solverParams = (Params.jumpFindingParams nmax) { precision = 768 }
      , boundConfig = defaultBoundConfig
      }

boundsProgram "GNYSigPsiEps_OPESearchTest_ngroup4_nmax6" =
  local (setJobType (MPIJob 2 36) . setJobTime (24*hour) . setJobMemory "45G" . setSlurmQos "nothrottle" . setSlurmPartition "pi_poland,day,scavenge") $ void $ do
  checkpointMap <- Bound.newCheckpointMap affine deltaExtToV Nothing
  lambdaMap     <- Bound.newLambdaMap affine deltaExtToV Nothing
  mapConcurrently_ (remoteGNYSigPsiEpsOPESearch checkpointMap lambdaMap initialBilinearForms . bound) $
    fmap toV [(417462859 / 400000000,60721879 / 80000000,1522959667 / 800000000)] --N4.gny4AllowedPts
  where
    nmax = 6
    ngroup = 4
    affine = N4.nmax4affine3d--gny2Affine3dNmax6
    bilinearMat :: Matrix 4 4 Rational
    bilinearMat = toM
     ((-1,0,0,0)
     ,(0,-1,0,0)
     ,(0,0,-1,0)
     ,(0,0,0,0.01))
    initialBilinearForms = BilinearForms 1e-32 [(Nothing, bilinearMat)]
    mLambda :: Maybe (V 4 Rational)
    mLambda = Just $ toV (0.43984004785635333, 0.1499339661556141,1, 1.803900477723386)--(1,1,1,1)
    bound deltaExts = Bound
      { boundKey = gnySigPsiEpsFeasibleDefaultGaps deltaExts mLambda ngroup nmax
      , precision = B3d.precision (Params.block3dParamsNmax nmax)
      , solverParams = (Params.jumpFindingParams nmax) { precision = 768 }
      , boundConfig = defaultBoundConfig
      }

boundsProgram "GNYSigPsiEps_delaunay_OPEScan_ngroup4_nmax6" =
  gnyDelaunaySearchOPEScan ngroup searchData
  where
    ngroup = 4
    lambda :: V 4 Rational
    lambda = toV (-(16262657/72101409),-(4851359/60948179),-(46012685/90323686),-(47127617/57015308))
    searchData = GNYDelaunaySearchData
        { nmax = 6
        , affine = N4.nmax4affine3d
        , initialLambda = lambda
        , initialCheckpoint = Nothing
        , initialDisallowed = [] --N4.nmax6DualGrid3d
        , initialAllowed = N4.nmax6PrimalGrid3d
        , opeEllipse = toM
                      ((-1,0,0,0)
                      ,(0,-1,0,0)
                      ,(0,0,-1,0)
                      ,(0,0,0,0.01))
        , solverPrecision = 768
        , delaunayConfig = defaultDelaunayConfig 16 600
        , jobTime = 8*hour
        , jobType = MPIJob 1 36
        , jobMemory = "50G"
        }

boundsProgram "GNYSigPsiEps_OPESearchTest_ngroup4_nmax8" =
  local (setJobType (MPIJob 2 24) . setJobTime (8*hour) . setJobMemory "100G" . setSlurmPartition "compute") $ void $ do
  checkpointMap <- Bound.newCheckpointMap affine deltaExtToV Nothing
  lambdaMap     <- Bound.newLambdaMap affine deltaExtToV Nothing
  mapConcurrently_ (remoteGNYSigPsiEpsOPESearch checkpointMap lambdaMap initialBilinearForms . bound) $
    fmap (apply affine . toV) [(x,y,z) | x <- [-0.75,-0.25..0.75], y <- [-0.75,-0.25..0.75], z <- [-0.75,-0.25..0.75] ] --N4.gny4AllowedPts
  where
    nmax = 8
    ngroup = 4
    affine = N4.nmax4affine3d--gny2Affine3dNmax6
    bilinearMat :: Matrix 4 4 Rational
    bilinearMat = N4.nmax6opeEllipse
    initialBilinearForms = BilinearForms 1e-32 [(Nothing, bilinearMat)]
    mLambda :: Maybe (V 4 Rational)
    mLambda = Just $ toV (0.43984004785635333, 0.1499339661556141,1, 1.803900477723386)--(1,1,1,1)
    bound deltaExts = Bound
      { boundKey = gnySigPsiEpsFeasibleDefaultGaps deltaExts mLambda ngroup nmax
      , precision = B3d.precision (Params.block3dParamsNmax nmax)
      , solverParams = (Params.jumpFindingParams nmax) { precision = 768 }
      , boundConfig = defaultBoundConfig
      }

boundsProgram "GNYSigPsiEps_OPESearchTest_ngroup4_nmax10" =
  local (setJobType (MPIJob 4 24) . setJobTime (8*hour) . setJobMemory "100G" . setSlurmPartition "compute") $ void $ do
  checkpointMap <- Bound.newCheckpointMap affine deltaExtToV Nothing
  lambdaMap     <- Bound.newLambdaMap affine deltaExtToV Nothing
  mapConcurrently_ (remoteGNYSigPsiEpsOPESearch checkpointMap lambdaMap initialBilinearForms . bound) $
    fmap (apply affine . toV) [(x,y,z) | x <- [-0.75,-0.25..0.75], y <- [-0.75,-0.25..0.75], z <- [-0.75,-0.25..0.75] ] --N4.gny4AllowedPts
  where
    nmax = 10
    ngroup = 4
    affine = N4.nmax4affine3d--gny2Affine3dNmax6
    bilinearMat :: Matrix 4 4 Rational
    bilinearMat = N4.nmax6opeEllipse
    initialBilinearForms = BilinearForms 1e-32 [(Nothing, bilinearMat)]
    mLambda :: Maybe (V 4 Rational)
    mLambda = Just $ toV (0.43984004785635333, 0.1499339661556141,1, 1.803900477723386)--(1,1,1,1)
    bound deltaExts = Bound
      { boundKey = gnySigPsiEpsFeasibleDefaultGaps deltaExts mLambda ngroup nmax
      , precision = B3d.precision (Params.block3dParamsNmax nmax)
      , solverParams = (Params.jumpFindingParams nmax) { precision = 768 }
      , boundConfig = defaultBoundConfig
      }

boundsProgram "GNYSigPsiEps_OPESearchGrid_ngroup4_nmax8" =
  local (setJobType (MPIJob 1 18) . setJobTime (24*hour) . setJobMemory "80G" . setSlurmPartition "pi_poland,day,scavenge" . setSlurmQos "nothrottle") $ void $ do
  checkpointMap <- Bound.newCheckpointMap affine deltaExtToV Nothing
  lambdaMap     <- Bound.newLambdaMap affine deltaExtToV Nothing
  mapConcurrently_ (remoteGNYSigPsiEpsOPESearch checkpointMap lambdaMap initialBilinearForms . bound) $ fmap (apply affine . toV)
    [(x,y,z) | x <- [-3/4,-1/2..3/4], y <- [-3/4,-1/2..3/4], z <- [-3/4,-1/2..3/4]] --n2nmax6PrimalPoints --GNYData.epsN2nmax6DualPoints --[(1.06 + x/200, 0.6 + y/50, 1.3 + z/10) | x <-[0..4], y <- [0..5], z <- [0..7]]
  where
    nmax = 8
    ngroup = 4
    affine = N4.nmax6affine--gny2Affine3dNmax6
    bilinearMat :: Matrix 4 4 Rational
    bilinearMat = N4.nmax6opeEllipse2
    initialBilinearForms = BilinearForms 1e-32 [(Nothing, bilinearMat)]
    mLambda :: Maybe (V 4 Rational)
    mLambda = Just $ toV (0.43964944628110225, 0.15047635286112315, 1, 1.7250097800457072) --(-0.258492, -0.123751, -0.515709, -0.807397) --(1,1,1,1)
    bound deltaExts = Bound
      { boundKey = gnySigPsiEpsFeasibleDefaultGaps deltaExts mLambda ngroup nmax
      , precision = B3d.precision (Params.block3dParamsNmax nmax)
      , solverParams = (Params.jumpFindingParams nmax) { precision = 768 }
      , boundConfig = defaultBoundConfig
      }

boundsProgram "GNYSigPsiEps_OPESearchGrid_ngroup4_nmax10" =
  local (setJobType (MPIJob 1 18) . setJobTime (24*hour) . setJobMemory "80G" . setSlurmPartition "pi_poland,day,scavenge" . setSlurmQos "nothrottle") $ void $ do
  checkpointMap <- Bound.newCheckpointMap affine deltaExtToV Nothing
  lambdaMap     <- Bound.newLambdaMap affine deltaExtToV Nothing
  mapConcurrently_ (remoteGNYSigPsiEpsOPESearch checkpointMap lambdaMap initialBilinearForms . bound) $ fmap (apply affine . toV)
    [(x,y,z) | x <- [-3/4,-1/2..3/4], y <- [-3/4,-1/2..3/4], z <- [-3/4,-1/2..3/4]] --n2nmax6PrimalPoints --GNYData.epsN2nmax6DualPoints --[(1.06 + x/200, 0.6 + y/50, 1.3 + z/10) | x <-[0..4], y <- [0..5], z <- [0..7]]
  where
    nmax = 10
    ngroup = 4
    affine = N4.nmax6affine--gny2Affine3dNmax6
    bilinearMat :: Matrix 4 4 Rational
    bilinearMat = N4.nmax6opeEllipse2
    initialBilinearForms = BilinearForms 1e-32 [(Nothing, bilinearMat)]
    mLambda :: Maybe (V 4 Rational)
    mLambda = Just $ toV (0.43964944628110225, 0.15047635286112315, 1, 1.7250097800457072) --(-0.258492, -0.123751, -0.515709, -0.807397) --(1,1,1,1)
    bound deltaExts = Bound
      { boundKey = gnySigPsiEpsFeasibleDefaultGaps deltaExts mLambda ngroup nmax
      , precision = B3d.precision (Params.block3dParamsNmax nmax)
      , solverParams = (Params.jumpFindingParams nmax) { precision = 768 }
      , boundConfig = defaultBoundConfig
      }

boundsProgram "GNYSigPsiEps_OPESearchGrid_ngroup4_nmax14" =
  local (setJobType (MPIJob 2 36) . setJobTime (24*hour) . setJobMemory "140G" . setSlurmPartition "pi_poland,day,scavenge" . setSlurmQos "nothrottle") $ void $ do
  checkpointMap <- Bound.newCheckpointMap affine deltaExtToV Nothing
  lambdaMap     <- Bound.newLambdaMap affine deltaExtToV Nothing
  mapConcurrently_ (remoteGNYSigPsiEpsOPESearch checkpointMap lambdaMap initialBilinearForms . bound) $ fmap (apply affine . toV)
    [(x,y,z) | x <- [1/4,1/2,3/4], y <- [0,1/4,1/2,3/4], z <- [-1/4,0,1/4]] --n2nmax6PrimalPoints --GNYData.epsN2nmax6DualPoints --[(1.06 + x/200, 0.6 + y/50, 1.3 + z/10) | x <-[0..4], y <- [0..5], z <- [0..7]]
  where
    nmax = 14
    ngroup = 4
    affine = N4.nmax10affine--gny2Affine3dNmax6
    bilinearMat :: Matrix 4 4 Rational
    bilinearMat = N4.nmax10OpeEllipse
    initialBilinearForms = BilinearForms 1e-32 [(Nothing, bilinearMat)]
    mLambda :: Maybe (V 4 Rational)
    mLambda = Just $ toV (0.4382241499686593, 0.15501744075953588, 1, 1.6723083580579237) --(-0.258492, -0.123751, -0.515709, -0.807397) --(1,1,1,1)
    bound deltaExts = Bound
      { boundKey = gnySigPsiEpsFeasibleDefaultGaps deltaExts mLambda ngroup nmax
      , precision = B3d.precision (Params.block3dParamsNmax nmax)
      , solverParams = (Params.jumpFindingParams nmax) { precision = 768 }
      , boundConfig = defaultBoundConfig
      }

boundsProgram "GNYSigPsiEps_OPESearchTest_node1/2_ngroup4_nmax14" =
  local (setJobType (MPIJob 1 64) . setJobTime (2*hour) . setJobMemory "128G" . setSlurmPartition "shared") $ void $ do
  checkpointMap <- Bound.newCheckpointMap affine deltaExtToV Nothing
  lambdaMap     <- Bound.newLambdaMap affine deltaExtToV Nothing
  mapConcurrently_ (remoteGNYSigPsiEpsOPESearch checkpointMap lambdaMap initialBilinearForms . bound) $ toV <$>
    [(10435737603980766407/10000000000000000000, 3785533226258124851/5000000000000000000, 75819732650819472601/40000000000000000000)] --N4.gny4AllowedPts
  where
    nmax = 14
    ngroup = 4
    affine = N4.nmax4affine3d--gny2Affine3dNmax6
    bilinearMat :: Matrix 4 4 Rational
    bilinearMat = N4.nmax10OpeEllipse
    initialBilinearForms = BilinearForms 1e-32 [(Nothing, bilinearMat)]
    mLambda :: Maybe (V 4 Rational)
    mLambda = Just $ toV (0.4382241499686593, 0.15501744075953588, 1, 1.6723083580579237)--(1,1,1,1)
    bound deltaExts = Bound
      { boundKey = gnySigPsiEpsFeasibleDefaultGaps deltaExts mLambda ngroup nmax
      , precision = B3d.precision (Params.block3dParamsNmax nmax)
      , solverParams = (Params.jumpFindingParams nmax) { precision = 768
                                                , procGranularity = 8 }
      , boundConfig = defaultBoundConfig
      }

boundsProgram "GNYSigPsiEps_OPESearchTest_node127/128_ngroup4_nmax14" =
  local (setJobType (MPIJob 1 100) . setJobTime (2*hour) . setJobMemory "200G" . setSlurmPartition "shared") $ void $ do
  checkpointMap <- Bound.newCheckpointMap affine deltaExtToV Nothing
  lambdaMap     <- Bound.newLambdaMap affine deltaExtToV Nothing
  mapConcurrently_ (remoteGNYSigPsiEpsOPESearch checkpointMap lambdaMap initialBilinearForms . bound) $ toV <$>
    [(10435737603980766407/10000000000000000000, 3785533226258124851/5000000000000000000, 75819732650819472601/40000000000000000000)] --N4.gny4AllowedPts
  where
    nmax = 14
    ngroup = 4
    affine = N4.nmax4affine3d--gny2Affine3dNmax6
    bilinearMat :: Matrix 4 4 Rational
    bilinearMat = N4.nmax10OpeEllipse
    initialBilinearForms = BilinearForms 1e-32 [(Nothing, bilinearMat)]
    mLambda :: Maybe (V 4 Rational)
    mLambda = Just $ toV (0.4382241499686593, 0.15501744075953588, 1, 1.6723083580579237)--(1,1,1,1)
    bound deltaExts = Bound
      { boundKey = gnySigPsiEpsFeasibleDefaultGaps deltaExts mLambda ngroup nmax
      , precision = B3d.precision (Params.block3dParamsNmax nmax)
      , solverParams = (Params.jumpFindingParams nmax) { precision = 768 }
      , boundConfig = defaultBoundConfig
      }

boundsProgram "GNYSigPsiEps_OPESearchTest_large-shared_ngroup4_nmax14" =
  local (setJobType (MPIJob 1 128) . setJobTime (2*hour) . setJobMemory "512G" . setSlurmPartition "large-shared") $ void $ do
  checkpointMap <- Bound.newCheckpointMap affine deltaExtToV Nothing
  lambdaMap     <- Bound.newLambdaMap affine deltaExtToV Nothing
  mapConcurrently_ (remoteGNYSigPsiEpsOPESearch checkpointMap lambdaMap initialBilinearForms . bound) $ toV <$>
    [(10435737603980766407/10000000000000000000, 3785533226258124851/5000000000000000000, 75819732650819472601/40000000000000000000)] --N4.gny4AllowedPts
  where
    nmax = 14
    ngroup = 4
    affine = N4.nmax4affine3d--gny2Affine3dNmax6
    bilinearMat :: Matrix 4 4 Rational
    bilinearMat = N4.nmax10OpeEllipse
    initialBilinearForms = BilinearForms 1e-32 [(Nothing, bilinearMat)]
    mLambda :: Maybe (V 4 Rational)
    mLambda = Just $ toV (0.4382241499686593, 0.15501744075953588, 1, 1.6723083580579237)--(1,1,1,1)
    bound deltaExts = Bound
      { boundKey = gnySigPsiEpsFeasibleDefaultGaps deltaExts mLambda ngroup nmax
      , precision = B3d.precision (Params.block3dParamsNmax nmax)
      , solverParams = (Params.jumpFindingParams nmax) { precision = 768 }
      , boundConfig = defaultBoundConfig
      }

boundsProgram "GNYSigPsiEps_OPESearchTest_node1_ngroup4_nmax14" =
  local (setJobType (MPIJob 1 128) . setJobTime (2*hour) . setJobMemory "250G" . setSlurmPartition "compute") $ void $ do
  checkpointMap <- Bound.newCheckpointMap affine deltaExtToV Nothing
  lambdaMap     <- Bound.newLambdaMap affine deltaExtToV Nothing
  mapConcurrently_ (remoteGNYSigPsiEpsOPESearch checkpointMap lambdaMap initialBilinearForms . bound) $ toV <$>
    [(10435737603980766407/10000000000000000000, 3785533226258124851/5000000000000000000, 75819732650819472601/40000000000000000000)] --N4.gny4AllowedPts
  where
    nmax = 14
    ngroup = 4
    affine = N4.nmax4affine3d--gny2Affine3dNmax6
    bilinearMat :: Matrix 4 4 Rational
    bilinearMat = N4.nmax10OpeEllipse
    initialBilinearForms = BilinearForms 1e-32 [(Nothing, bilinearMat)]
    mLambda :: Maybe (V 4 Rational)
    mLambda = Just $ toV (0.4382241499686593, 0.15501744075953588, 1, 1.6723083580579237)--(1,1,1,1)
    bound deltaExts = Bound
      { boundKey = gnySigPsiEpsFeasibleDefaultGaps deltaExts mLambda ngroup nmax
      , precision = B3d.precision (Params.block3dParamsNmax nmax)
      , solverParams = (Params.jumpFindingParams nmax) { precision = 768
                                                , procGranularity = 4 }
      , boundConfig = defaultBoundConfig
      }

boundsProgram "GNYSigPsiEps_OPESearchTest_node2_ngroup4_nmax14" =
  local (setJobType (MPIJob 2 128) . setJobTime (2*hour) . setJobMemory "250G" . setSlurmPartition "compute") $ void $ do
  checkpointMap <- Bound.newCheckpointMap affine deltaExtToV Nothing
  lambdaMap     <- Bound.newLambdaMap affine deltaExtToV Nothing
  mapConcurrently_ (remoteGNYSigPsiEpsOPESearch checkpointMap lambdaMap initialBilinearForms . bound) $ toV <$>
    [(10435737603980766407/10000000000000000000, 3785533226258124851/5000000000000000000, 75819732650819472601/40000000000000000000)] --N4.gny4AllowedPts
  where
    nmax = 14
    ngroup = 4
    affine = N4.nmax4affine3d--gny2Affine3dNmax6
    bilinearMat :: Matrix 4 4 Rational
    bilinearMat = N4.nmax10OpeEllipse
    initialBilinearForms = BilinearForms 1e-32 [(Nothing, bilinearMat)]
    mLambda :: Maybe (V 4 Rational)
    mLambda = Just $ toV (0.4382241499686593, 0.15501744075953588, 1, 1.6723083580579237)--(1,1,1,1)
    bound deltaExts = Bound
      { boundKey = gnySigPsiEpsFeasibleDefaultGaps deltaExts mLambda ngroup nmax
      , precision = B3d.precision (Params.block3dParamsNmax nmax)
      , solverParams = (Params.jumpFindingParams nmax) { precision = 768
                                                , procGranularity = 2 }
      , boundConfig = defaultBoundConfig
      }

boundsProgram "GNYSigPsiEps_delaunay_OPEScan_ngroup4_nmax14" =
  gnyDelaunaySearchOPEScan ngroup searchData
  where
    ngroup = 4
    lambda :: V 4 Rational
    lambda = toV (0.4382241499686593, 0.15501744075953588, 1, 1.6723083580579237)
    searchData = GNYDelaunaySearchData
        { nmax = 14
        , affine = N4.nmax10affine
        , initialLambda = lambda
        , initialCheckpoint = Just "/expanse/lustre/scratch/rse/temp_project/data/2021-07/KcDjr/Object_ZEf9Xt39Ns4A_Abh4AEK7zNx8Qkv4oN21YY5383UB5g/ck"
        , initialDisallowed = N4.nmax14Dual
        , initialAllowed = N4.nmax14Primal
        , opeEllipse = N4.nmax10OpeEllipse
        , solverPrecision = 768
        , delaunayConfig = defaultDelaunayConfig 4 100
        , jobTime = 24*hour
        , jobType = MPIJob 1 128
        , jobMemory = "250G"
        }

-----------
--- N=8 ---
-----------

boundsProgram "GNYSigPsiEps_test_ngroup8_nmax6" =
  local (setJobType (MPIJob 1 36) . setJobTime (4*hour) . setSlurmPartition "pi_poland,day,scavenge") $ void $ do
  mapConcurrently_ (Bound.remoteCompute . bound) $
     fmap toV $ [(1.02136,0.86447,1.9)] --n2nmax6PrimalPoints --GNYData.epsN2nmax6DualPoints --[(1.06 + x/200, 0.6 + y/50, 1.3 + z/10) | x <-[0..4], y <- [0..5], z <- [0..7]]
  where
    nmax = 6
    ngroup = 8
    bound deltaExts = Bound
      { boundKey = gnySigPsiEpsFeasibleDefaultGaps deltaExts Nothing ngroup nmax
      , precision = B3d.precision (Params.block3dParamsNmax nmax)
      , solverParams = (Params.jumpFindingParams nmax) { precision = 768
                                             , dualErrorThreshold=1e-30
                                             , primalErrorThreshold=1e-40
                                             , findPrimalFeasible = False }
      , boundConfig = defaultBoundConfig
      }

boundsProgram "GNYSigPsiEps_test_ngroup8_nmax10" =
  local (setJobType (MPIJob 1 128) . setJobTime (1*hour) . setSlurmPartition "compute" . setJobMemory "240G") $ void $ do
  mapConcurrently_ (Bound.remoteCompute . bound) $
     toV <$> [(1.02136,0.86447,1.9)] --n2nmax6PrimalPoints --GNYData.epsN2nmax6DualPoints --[(1.06 + x/200, 0.6 + y/50, 1.3 + z/10) | x <-[0..4], y <- [0..5], z <- [0..7]]
  where
    nmax = 10
    ngroup = 8
    bound deltaExts = Bound
      { boundKey = gnySigPsiEpsFeasibleDefaultGaps deltaExts Nothing ngroup nmax
      , precision = B3d.precision (Params.block3dParamsNmax nmax)
      , solverParams = (Params.jumpFindingParams nmax) { precision = 768
                                             , dualErrorThreshold=1e-30
                                             , primalErrorThreshold=1e-40
                                             , findPrimalFeasible = False }
      , boundConfig = defaultBoundConfig
      }

boundsProgram "GNYSigPsiEps_OPESearchTest_ngroup8_nmax10" =
  local (setJobType (MPIJob 1 36) . setJobTime (8*hour) . setJobMemory "150G" . setSlurmPartition "scavenge") $ void $ do
  checkpointMap <- Bound.newCheckpointMap affine deltaExtToV Nothing
  lambdaMap     <- Bound.newLambdaMap affine deltaExtToV Nothing
  mapConcurrently_ (remoteGNYSigPsiEpsOPESearch checkpointMap lambdaMap initialBilinearForms . bound) $ fmap (apply affine) $
     fmap toV [(0,0,0)] --n2nmax6PrimalPoints --GNYData.epsN2nmax6DualPoints --[(1.06 + x/200, 0.6 + y/50, 1.3 + z/10) | x <-[0..4], y <- [0..5], z <- [0..7]]
  where
    nmax = 10
    ngroup = 8
    affine = N8.nmax6affine3d--gny2Affine3dNmax6
    bilinearMat :: Matrix 4 4 Rational
    bilinearMat = toM
     ((-1,0,0,0)
     ,(0,-1,0,0)
     ,(0,0,-1,0)
     ,(0,0,0,0.01))
--
    initialBilinearForms = BilinearForms 1e-32 [(Nothing, bilinearMat)]
    mLambda :: Maybe (V 4 Rational)
    mLambda = Just $ toV (0.3501, 0.165, 0.6787, 1) --(-0.258492, -0.123751, -0.515709, -0.807397) --(1,1,1,1)
    bound deltaExts = Bound
      { boundKey = gnySigPsiEpsFeasibleDefaultGaps deltaExts mLambda ngroup nmax
      , precision = B3d.precision (Params.block3dParamsNmax nmax)
      , solverParams = (Params.jumpFindingParams nmax) { precision = 2048 }
      , boundConfig = defaultBoundConfig
      }

boundsProgram "GNYSigPsiEps_OPESearchGrid_ngroup8_nmax6" =
  local (setJobType (MPIJob 1 9) . setJobTime (8*hour) . setJobMemory "25G" . setSlurmPartition "pi_poland,day,scavenge" . setSlurmQos "nothrottle") $ void $ do
  checkpointMap <- Bound.newCheckpointMap affine deltaExtToV Nothing
  lambdaMap     <- Bound.newLambdaMap affine deltaExtToV Nothing
  mapConcurrently_ (remoteGNYSigPsiEpsOPESearch checkpointMap lambdaMap initialBilinearForms . bound) $ fmap (apply affine . toV)
    [(x,y,z) | x <- [-1,-2/3..1], y <- [-1,-2/3..1], z <- [-1,-2/3..1]] --n2nmax6PrimalPoints --GNYData.epsN2nmax6DualPoints --[(1.06 + x/200, 0.6 + y/50, 1.3 + z/10) | x <-[0..4], y <- [0..5], z <- [0..7]]
  where
    nmax = 6
    ngroup = 8
    affine = N8.nmax6affine3d--gny2Affine3dNmax6
    bilinearMat :: Matrix 4 4 Rational
    bilinearMat = N8.nmax6opeEllipse
--
    initialBilinearForms = BilinearForms 1e-32 [(Nothing, bilinearMat)]
    mLambda :: Maybe (V 4 Rational)
    mLambda = Just $ toV (0.147354, -0.0359405, -0.444498, -0.880165) --(-0.258492, -0.123751, -0.515709, -0.807397) --(1,1,1,1)
    bound deltaExts = Bound
      { boundKey = gnySigPsiEpsFeasibleDefaultGaps deltaExts mLambda ngroup nmax
      , precision = B3d.precision (Params.block3dParamsNmax nmax)
      , solverParams = (Params.jumpFindingParams nmax) { precision = 768 }
      , boundConfig = defaultBoundConfig
      }

boundsProgram "GNYSigPsiEps_delaunay_OPEScan_ngroup8_nmax4" =
  gnyDelaunaySearchOPEScan ngroup searchData
  where
    ngroup = 8
    lambda :: V 4 Rational
    lambda = toV (0.334923, 0.0806146,1, 1.85786)
    searchData = GNYDelaunaySearchData
        { nmax = 4
        , affine = N8.nmax6affine3d
        , initialLambda = lambda
        , initialCheckpoint = Nothing
        , initialDisallowed = []--N8.nmax6dual3d
        , initialAllowed = fmap toV [(510455899 / 500000000,10852644101 / 12500000000,992069883433 / 500000000000)]
        , opeEllipse = N8.nmax6opeEllipse
        , solverPrecision = 768
        , delaunayConfig = defaultDelaunayConfig 8 600
        , jobTime = 8*hour
        , jobType = MPIJob 2 24
        , jobMemory = "30G"
        }

boundsProgram "GNYSigPsiEps_delaunay_OPEScan_ngroup8_nmax6" =
  gnyDelaunaySearchOPEScan ngroup searchData
  where
    ngroup = 8
    lambda :: V 4 Rational
    lambda = toV (0.147354, -0.0359405, -0.444498, -0.880165)
    searchData = GNYDelaunaySearchData
        { nmax = 6
        , affine = N8.nmax6affine253d
        , initialLambda = lambda
        , initialCheckpoint = Nothing
        , initialDisallowed = N8.nmax6dual3d
        , initialAllowed = N8.nmax6primal3d
        , opeEllipse = N8.nmax6opeEllipse
        , solverPrecision = 768
        , delaunayConfig = defaultDelaunayConfig 16 600
        , jobTime = 8*hour
        , jobType = MPIJob 1 32
        , jobMemory = "64G"
        }

boundsProgram "GNYSigPsiEps_delaunay_OPEScan_ngroup8_nmax10" =
  gnyDelaunaySearchOPEScan ngroup searchData
  where
    ngroup = 8
    lambda :: V 4 Rational
    lambda = toV (0.33483550031059134, 0.08038022485850224, 1, 1.736978461652809)
    searchData = GNYDelaunaySearchData
        { nmax = 10
        , affine = N8.nmax6affine253d
        , initialLambda = lambda
        , initialCheckpoint = Nothing
        , initialDisallowed = N8.nmax6dual3d
        , initialAllowed = N8.nmax6primal3d
        , opeEllipse = N8.nmax6DelaunayOpeEllipse
        , solverPrecision = 768
        , delaunayConfig = defaultDelaunayConfig 4 100
        , jobTime = 8*hour
        , jobType = MPIJob 1 32
        , jobMemory = "64G"
        }

boundsProgram "GNYSigPsiEps_OPESearchGrid_ngroup8_nmax10" =
  local (setJobType (MPIJob 1 36) . setJobTime (8*hour) . setJobMemory "100G" . setSlurmPartition "pi_poland,day,scavenge" . setSlurmQos "nothrottle") $ void $ do
  checkpointMap <- Bound.newCheckpointMap affine deltaExtToV Nothing
  lambdaMap     <- Bound.newLambdaMap affine deltaExtToV Nothing
  mapConcurrently_ (remoteGNYSigPsiEpsOPESearch checkpointMap lambdaMap initialBilinearForms . bound) $ fmap (apply affine . toV)
    [(x,y,z) | x <- [0], y <- [0], z <- [-1/4,0..1]] --n2nmax6PrimalPoints --GNYData.epsN2nmax6DualPoints --[(1.06 + x/200, 0.6 + y/50, 1.3 + z/10) | x <-[0..4], y <- [0..5], z <- [0..7]]
  where
    nmax = 10
    ngroup = 8
    affine = N8.nmax8affine--gny2Affine3dNmax6
    bilinearMat :: Matrix 4 4 Rational
    bilinearMat = N8.nmax6ope25Ellipse
    initialBilinearForms = BilinearForms 1e-32 [(Nothing, bilinearMat)]
    mLambda :: Maybe (V 4 Rational)
    mLambda = Just $ toV (0.147354, -0.0359405, -0.444498, -0.880165) --(-0.258492, -0.123751, -0.515709, -0.807397) --(1,1,1,1)
    bound deltaExts = Bound
      { boundKey = gnySigPsiEpsFeasibleDefaultGaps deltaExts mLambda ngroup nmax
      , precision = B3d.precision (Params.block3dParamsNmax nmax)
      , solverParams = (Params.jumpFindingParams nmax) { precision = 768 }
      , boundConfig = defaultBoundConfig
      }

boundsProgram "GNYSigPsiEps_OPESearchGrid_ngroup8_nmax12" =
  local (setJobType (MPIJob 1 36) . setJobTime (24*hour) . setJobMemory "140G" . setSlurmPartition "pi_poland,day,scavenge" . setSlurmQos "nothrottle") $ void $ do
  checkpointMap <- Bound.newCheckpointMap affine deltaExtToV Nothing
  lambdaMap     <- Bound.newLambdaMap affine deltaExtToV Nothing
  mapConcurrently_ (remoteGNYSigPsiEpsOPESearch checkpointMap lambdaMap initialBilinearForms . bound) $ fmap (apply affine . toV)
    [(x,y,z) | x <- [0,1/4,1/2,3/4], y <- [0,1/4,1/2,3/4], z <- [-1/4,0,1/4]] --n2nmax6PrimalPoints --GNYData.epsN2nmax6DualPoints --[(1.06 + x/200, 0.6 + y/50, 1.3 + z/10) | x <-[0..4], y <- [0..5], z <- [0..7]]
  where
    nmax = 12
    ngroup = 8
    affine = N8.nmax10affine--gny2Affine3dNmax6
    bilinearMat :: Matrix 4 4 Rational
    bilinearMat = N8.nmax10OpeEllipse
    initialBilinearForms = BilinearForms 1e-32 [(Nothing, bilinearMat)]
    mLambda :: Maybe (V 4 Rational)
    mLambda = Just $ toV (0.3323789444949851, 0.08081765136416777, 1, 1.6990274416890423) --(-0.258492, -0.123751, -0.515709, -0.807397) --(1,1,1,1)
    bound deltaExts = Bound
      { boundKey = gnySigPsiEpsFeasibleDefaultGaps deltaExts mLambda ngroup nmax
      , precision = B3d.precision (Params.block3dParamsNmax nmax)
      , solverParams = (Params.jumpFindingParams nmax) { precision = 768 }
      , boundConfig = defaultBoundConfig
      }

boundsProgram "GNYSigPsiEps_OPESearchGrid_ngroup8_nmax14" =
  local (setJobType (MPIJob 2 36) . setJobTime (24*hour) . setJobMemory "140G" . setSlurmPartition "pi_poland,day,scavenge" . setSlurmQos "nothrottle") $ void $ do
  checkpointMap <- Bound.newCheckpointMap affine deltaExtToV Nothing
  lambdaMap     <- Bound.newLambdaMap affine deltaExtToV Nothing
  mapConcurrently_ (remoteGNYSigPsiEpsOPESearch checkpointMap lambdaMap initialBilinearForms . bound) $ fmap (apply affine . toV)
    [(x,y,z) | x <- [1/4,1/2,3/4], y <- [0,1/4,1/2,3/4], z <- [-1/4,0,1/4]] --n2nmax6PrimalPoints --GNYData.epsN2nmax6DualPoints --[(1.06 + x/200, 0.6 + y/50, 1.3 + z/10) | x <-[0..4], y <- [0..5], z <- [0..7]]
  where
    nmax = 14
    ngroup = 8
    affine = N8.nmax10affine--gny2Affine3dNmax6
    bilinearMat :: Matrix 4 4 Rational
    bilinearMat = N8.nmax10OpeEllipse
    initialBilinearForms = BilinearForms 1e-32 [(Nothing, bilinearMat)]
    mLambda :: Maybe (V 4 Rational)
    mLambda = Just $ toV (0.3323789444949851, 0.08081765136416777, 1, 1.6990274416890423) --(-0.258492, -0.123751, -0.515709, -0.807397) --(1,1,1,1)
    bound deltaExts = Bound
      { boundKey = gnySigPsiEpsFeasibleDefaultGaps deltaExts mLambda ngroup nmax
      , precision = B3d.precision (Params.block3dParamsNmax nmax)
      , solverParams = (Params.jumpFindingParams nmax) { precision = 768 }
      , boundConfig = defaultBoundConfig
      }

boundsProgram "GNYSigPsiEps_delaunay_OPEScan_ngroup8_nmax14" =
  gnyDelaunaySearchOPEScan ngroup searchData
  where
    ngroup = 8
    lambda :: V 4 Rational
    lambda = toV (0.3323789444949851, 0.08081765136416777, 1, 1.6990274416890423)
    searchData = GNYDelaunaySearchData
        { nmax = 14
        , affine = N8.nmax10affine
        , initialLambda = lambda
        , initialCheckpoint = Just "/expanse/lustre/scratch/rse/temp_project/data/2021-07/oXjvG/Object_-RQiKlzBsJIMMEKa0ej5ApJcVoTqdzDy1cfegHMmVcw/ck"
        , initialDisallowed = N8.nmax14Dual
        , initialAllowed = N8.nmax14Primal
        , opeEllipse = N8.nmax10OpeEllipse
        , solverPrecision = 768
        , delaunayConfig = defaultDelaunayConfig 4 100
        , jobTime = 24*hour
        , jobType = MPIJob 1 128
        , jobMemory = "250G"
        }
--   local (setJobType (MPIJob 1 8) . setJobMemory "24G" . setJobTime (1*hour)) $ do
--   checkpointMap <- Bound.newCheckpointMap affine deltaExtEpsToV mInitialCheckpoint
--   let
--     f :: V 3 Rational -> Cluster Bool
--     f deltaV = fmap SDPB.isPrimalFeasible $
--       remoteComputeGNYBoundWithCheckpointMap checkpointMap $
--       bound deltaV
--   void $ delaunaySearchRegionPersistent delaunaySearchPoints delaunayConfig affine initialPts f
--   where
--     nmax = 8
--     ngroup = 2
--     affine = GNYData.gny2Affine3dNmax8
--     bound deltaV = Bound
--       { boundKey = gny3dSearchDefaultGaps deltaV ngroup nmax
--       , precision = B3d.precision (Params.block3dParamsNmax nmax)
--       , solverParams = (sdpbParamsNmax nmax) { precision = 768
--                                              , findPrimalFeasible = False }
--       , boundConfig = defaultBoundConfig
--       }
--     delaunayConfig = defaultDelaunayConfig 1 2
--     initialPts = Map.fromList $
--       [(p, Just True)  | p <- initialAllowed] ++
--       [(p, Just False) | p <- initialDisallowed]
--       where
--         initialDisallowed = fmap toV $ GNYData.n2nmax8DualPoints
--         initialAllowed = fmap toV $ GNYData.n2nmax8PrimalPoints
--     -- Instead of 'Nothing', we could supply a pre-computed checkpoint
--     -- here. It would be used as the default return value for the
--     -- checkpointMap, until at least one completed checkpoint gets
--     -- recorded.
--     mInitialCheckpoint = Nothing
--
-- boundsProgram "GNY_3d_island_ngroup2_nmax10_with_checkpoint_map" =
--   -- To run at Yale, probably best to set 8 CPUs -> 7 CPUs and 24GB -> 20GB
--   local (setJobType (MPIJob 1 24) . setJobMemory "72G" . setJobTime (8*hour)) $ do
--   checkpointMap <- Bound.newCheckpointMap affine deltaExtEpsToV mInitialCheckpoint
--   let
--     f :: V 3 Rational -> Cluster Bool
--     f deltaV = fmap SDPB.isPrimalFeasible $
--       remoteComputeGNYBoundWithCheckpointMap checkpointMap $
--       bound deltaV
--   void $ delaunaySearchRegionPersistent delaunaySearchPoints delaunayConfig affine initialPts f
--   where
--     nmax = 10
--     ngroup = 2
--     affine = GNYData.gny2Affine3dNmax10
--     bound deltaV = Bound
--       { boundKey = gny3dSearchDefaultGaps deltaV ngroup nmax
--       , precision = B3d.precision (Params.block3dParamsNmax nmax)
--       , solverParams = (sdpbParamsNmax nmax) { precision = 768
--                                              , findPrimalFeasible = False }
--       , boundConfig = defaultBoundConfig
--       }
--     delaunayConfig = defaultDelaunayConfig 10 100
--     initialPts = Map.fromList $
--       [(p, Just True)  | p <- initialAllowed] ++
--       [(p, Just False) | p <- initialDisallowed]
--       where
--         initialDisallowed = fmap toV $ GNYData.n2nmax10DualPoints2
--         initialAllowed = fmap toV $ GNYData.n2nmax10PrimalPoints2
--     -- Instead of 'Nothing', we could supply a pre-computed checkpoint
--     -- here. It would be used as the default return value for the
--     -- checkpointMap, until at least one completed checkpoint gets
--     -- recorded.
--     mInitialCheckpoint = Nothing  --"/central/groups/dssimmon/aike/test/data/2020-11/KJrLH/Object_uwnS1eRBtHdv-MEYw0V9aB0Jy_TTwoj-5qmLNVakeFg/ck"
--
-- boundsProgram "GNY_3d_island_ngroup2_nmax12_with_checkpoint_map" =
--   -- To run at Yale, probably best to set 8 CPUs -> 7 CPUs and 24GB -> 20GB
--   local (setJobType (MPIJob 1 32) . setJobMemory "96G" . setJobTime (24*hour)) $ do
--   checkpointMap <- Bound.newCheckpointMap affine deltaExtEpsToV mInitialCheckpoint
--   let
--     f :: V 3 Rational -> Cluster Bool
--     f deltaV = fmap SDPB.isPrimalFeasible $
--       remoteComputeGNYBoundWithCheckpointMap checkpointMap $
--       bound deltaV
--   void $ delaunaySearchRegionPersistent delaunaySearchPoints delaunayConfig affine initialPts f
--   where
--     nmax = 12
--     ngroup = 2
--     affine = GNYData.gny2Affine3dNmax12
--     bound deltaV = Bound
--       { boundKey = gny3dSearchDefaultGaps deltaV ngroup nmax
--       , precision = B3d.precision (Params.block3dParamsNmax nmax)
--       , solverParams = (sdpbParamsNmax nmax) { precision = 768
--                                              , findPrimalFeasible = False }
--       , boundConfig = defaultBoundConfig
--       }
--     delaunayConfig = defaultDelaunayConfig 20 300
--     initialPts = Map.fromList $
--       [(p, Just True)  | p <- initialAllowed] ++
--       [(p, Just False) | p <- initialDisallowed]
--       where
--         initialDisallowed = fmap toV $ GNYData.n2nmax12DualPoints
--         initialAllowed = fmap toV $ GNYData.n2nmax12PrimalPoints
--     -- Instead of 'Nothing', we could supply a pre-computed checkpoint
--     -- here. It would be used as the default return value for the
--     -- checkpointMap, until at least one completed checkpoint gets
--     -- recorded.
--     mInitialCheckpoint = Nothing




----------------
-- N=4 search --
----------------


--boundsProgram "GNY_3d_island_ngroup4_nmax6" =
--  local (setJobType (MPIJob 1 7) . setJobMemory "20G" . setJobTime (1*hour)) $ do
--  void $ delaunaySearchRegionPersistent delaunaySearchPoints delaunayConfig affine initialPts f
--  where
--    nmax = 6
--    ngroup = 4
--    affine = gny4Affine3dNmax6
--    f :: V 3 Rational -> Cluster Bool
--    f deltaV = fmap SDPB.isPrimalFeasible (remoteComputeGNYBound $ bound deltaV)
--    bound deltaV = Bound
--      { boundKey = gny3dSearchDefaultGaps deltaV ngroup nmax
--      , precision = B3d.precision (Params.block3dParamsNmax nmax)
--      , solverParams = (sdpbParamsNmax nmax) { precision = 768
--                                             , findPrimalFeasible = False }
--      , boundConfig = defaultBoundConfig
--      }
--    delaunayConfig = defaultDelaunayConfig 20 2000
--    initialPts = Map.fromList $
--      [(p, Just True)  | p <- initialAllowed] ++
--      [(p, Just False) | p <- initialDisallowed]
--      where
--        initialDisallowed = fmap toV []
--        initialAllowed = fmap toV [(1.044,0.77,1.8)]
--
--boundsProgram "GNY_3d_island_ngroup4_nmax6_with_checkpoint_map" =
--  -- To run at Yale, probably best to set 8 CPUs -> 7 CPUs and 24GB -> 20GB
--  local (setJobType (MPIJob 1 8) . setJobMemory "24G" . setJobTime (1*hour)) $ do
--  checkpointMap <- Bound.newCheckpointMap affine deltaExtEpsToV mInitialCheckpoint
--  let
--    f :: V 3 Rational -> Cluster Bool
--    f deltaV = fmap SDPB.isPrimalFeasible $
--      remoteComputeGNYBoundWithCheckpointMap checkpointMap $
--      bound deltaV
--  void $ delaunaySearchRegionPersistent delaunaySearchPoints delaunayConfig affine initialPts f
--  where
--    nmax = 6
--    ngroup = 4
--    affine = gny4Affine3dNmax6
--    bound deltaV = Bound
--      { boundKey = gny3dSearchDefaultGaps deltaV ngroup nmax
--      , precision = B3d.precision (Params.block3dParamsNmax nmax)
--      , solverParams = (sdpbParamsNmax nmax) { precision = 768
--                                             , findPrimalFeasible = False }
--      , boundConfig = defaultBoundConfig
--      }
--    delaunayConfig = defaultDelaunayConfig 20 2000
--    initialPts = Map.fromList $
--      [(p, Just True)  | p <- initialAllowed] ++
--      [(p, Just False) | p <- initialDisallowed]
--      where
--        initialDisallowed = fmap toV []
--        initialAllowed = fmap toV [(1.044,0.77,1.8)]
--    -- Instead of 'Nothing', we could supply a pre-computed checkpoint
--    -- here. It would be used as the default return value for the
--    -- checkpointMap, until at least one completed checkpoint gets
--    -- recorded.
--    mInitialCheckpoint = Nothing
--
--boundsProgram "GNY_3d_island_ngroup4_nmax8" =
--  local (setJobType (MPIJob 1 18) . setJobMemory "40G" . setJobTime (4*hour)) $ do
--  void $ delaunaySearchRegionPersistent delaunaySearchPoints delaunayConfig affine initialPts f
--  where
--    nmax = 8
--    ngroup = 4
--    affine = gny4Affine3dNmax6
--    f :: V 3 Rational -> Cluster Bool
--    f deltaV = fmap SDPB.isPrimalFeasible (remoteComputeGNYBound $ bound deltaV)
--    bound deltaV = Bound
--      { boundKey = gny3dSearchDefaultGaps deltaV ngroup nmax
--      , precision = B3d.precision (Params.block3dParamsNmax nmax)
--      , solverParams = (sdpbParamsNmax nmax) { precision = 768
--                                             , findPrimalFeasible = False }
--      , boundConfig = defaultBoundConfig
--      }
--    delaunayConfig = defaultDelaunayConfig 26 1000
--    initialPts = Map.fromList $
--      [(p, Just True)  | p <- initialAllowed] ++
--      [(p, Just False) | p <- initialDisallowed]
--      where
--        initialAllowed = fmap toV [(9341401777594260840536377/8947306799824896000000000,214138321200063753560017463/286313817594396672000000000,2367555402948089231516369081/1431569087971983360000000000)]
--
--boundsProgram "GNY_Island_ngroup4_nmax8" =
--  local (setJobType (MPIJob 1 9) . setJobMemory "50G" . setJobTime (2*hour)) $ do
--  void $ delaunaySearchRegionPersistent delaunaySearchPoints delaunayConfig affine initialPts f
--  where
--    nmax = 8
--    ngroup = 4
--    affine = gny4AffineNmax6
--    f :: V 2 Rational -> Cluster Bool
--    f deltaExts = fmap SDPB.isPrimalFeasible (remoteComputeGNYBound $ bound deltaExts)
--    bound deltaExts = Bound
--      { boundKey = gnyFeasibleDefaultGaps deltaExts ngroup nmax
--      , precision = B3d.precision (Params.block3dParamsNmax nmax)
--      , solverParams = (sdpbParamsNmax nmax) { precision = 768
--                                             , findPrimalFeasible = False }
--      , boundConfig = defaultBoundConfig
--      }
--    delaunayConfig = defaultDelaunayConfig 20 200
--    initialPts = Map.fromList $
--      [(p, Just True)  | p <- initialAllowed] ++
--      [(p, Just False) | p <- initialDisallowed]
--      where
--        initialDisallowed = fmap toV $
--          [(409/400, 3/4), (419/400, 5/8), (419/400, 7/8), (1717/1600, 21/32), (26/25, 23/32), (26/25, 25/32),
--          (417/400, 3/4), (13417/12800, 175/256), (13907/12800, 159/256), (27019/25600, 445/512),
--          (13421/12800, 213/256), (53773/51200, 691/1024), (667/640, 189/256), (13591/12800, 245/256),
--          (107763/102400, 1749/2048), (667/640, 195/256), (444069/409600, 5265/8192), (439919/409600, 5423/8192),
--          (213561/204800, 3011/4096), (53749/51200, 53/64), (53583/51200, 829/1024), (106737/102400, 1485/2048),
--          (887183/819200, 10707/16384), (1779077/1638400, 21233/32768), (106761/102400, 6211/8192),
--          (1766093/1638400, 21529/32768), (108317/102400, 339/512), (112077/102400, 1245/2048),
--          (225041/204800, 2463/4096), (108349/102400, 1859/2048), (214833/204800, 841/1024),
--          (85403/81920, 5871/8192), (1429953/1310720, 84529/131072), (34423/32768, 13677/16384),
--          (213807/204800, 2893/4096), (3449163/3276800, 55701/65536), (871923/819200, 32283/32768),
--          (6896029/6553600, 86631/131072), (2844053/2621440, 171461/262144), (697303/655360, 124831/131072),
--          (1737549/1638400, 59879/65536), (173461/163840, 14663/16384), (7078993/6553600, 86413/131072),
--          (28685061/26214400, 335985/524288), (853501/819200, 12811/16384), (3507567/3276800, 43437/65536),
--          (28505817/26214400, 343541/524288), (3603713/3276800, 39327/65536), (106883/102400, 811/1024),
--          (13791067/13107200, 221701/262144), (1380123/1310720, 87621/131072),
--          (115135429/104857600, 1333169/2097152), (27664593/26214400, 349571/524288),
--          (13887039/13107200, 174605/262144), (6964789/6553600, 245791/262144),
--          (114733593/104857600, 1361205/2097152), (341707/327680, 25633/32768),
--          (18441389/16777216, 5319313/8388608), (230230021/209715200, 2702193/4194304),
--          (2777197/2621440, 473535/524288), (2741027/2621440, 178921/262144), (5581587/5242880, 1013611/1048576),
--          (866271/819200, 10919/16384), (27568087/26214400, 354357/524288),
--          (919292983/838860800, 10921435/16777216), (1843816687/1677721600, 21634419/33554432),
--          (457561549/419430400, 5471257/8388608), (115312691/104857600, 1317549/2097152),
--          (13808413/13107200, 224403/262144), (11532699/10485760, 635737/1048576), (174789/163840, 32637/32768),
--          (227740279/209715200, 2738203/4194304), (13711891/13107200, 180613/262144),
--          (3684856907/3355443200, 43551647/67108864), (110440707/104857600, 1410681/2097152),
--          (55861347/52428800, 2047887/2097152), (27846429/26214400, 974439/1048576),
--          (6969181/6553600, 87225/131072), (230619257/209715200, 2694183/4194304),
--          (223354153/209715200, 8073533/8388608), (92257093/83886080, 5367879/8388608),
--          (563657/524288, 173165/262144), (1831714673/1677721600, 21859629/33554432),
--          (27412063/26214400, 363113/524288), (28244177/26214400, 346109/524288),
--         (110656503/104857600, 1862041/2097152)]
--        initialAllowed = [toV (1.044,0.77)]
--
--boundsProgram "GNY_test_ngroup4_nmax6" =
--  local (setJobType (MPIJob 1 36) . setJobTime (1*hour)) $ void $ do
--  mapConcurrently_ (remoteComputeGNYBound . bound) $
--     fmap toV [(1.044,0.77,1+x/10) | x <-[0..10]]
--  where
--    nmax = 6
--    ngroup = 4
--    bound deltaExts = Bound
--      { boundKey = gny3dSearchDefaultGaps deltaExts ngroup nmax
--      , precision = B3d.precision (Params.block3dParamsNmax nmax)
--      , solverParams = (sdpbParamsNmax nmax) { precision = 768
--                                             , findPrimalFeasible = False }
--      , boundConfig = defaultBoundConfig
--      }
--
--
--
--boundsProgram "SuperIsing_Island_nmax6" =
--  local (setJobType (MPIJob 1 9) . setJobMemory "50G" . setJobTime (1*hour)) $ do
--  void $ delaunaySearchRegionPersistent delaunaySearchPoints delaunayConfig affine initialPts f
--  where
--    nmax = 6
--    affine = superIsingAffineNmax6
--    f :: V 2 Rational -> Cluster Bool
--    f deltaExts = fmap SDPB.isPrimalFeasible (remoteComputeGNYN1Bound $ bound deltaExts)
--    bound deltaExts = Bound
--      { boundKey = superIsingFeasibleDefaultGaps deltaExts nmax
--      , precision = B3d.precision (Params.block3dParamsNmax nmax)
--      , solverParams = (sdpbParamsNmax nmax) { precision = 768
--                                             , findPrimalFeasible = False }
--      , boundConfig = defaultBoundConfig
--      }
--    delaunayConfig = defaultDelaunayConfig 20 200
--    initialPts = Map.fromList $
--      [(p, Just True)  | p <- initialAllowed] ++
--      [(p, Just False) | p <- initialDisallowed]
--      where
--        initialDisallowed = []
--        initialAllowed = [toV (1.085,0.6)]
--
--boundsProgram "BigJumpSearch_nmax6" =
--  local (setJobType (MPIJob 1 9) . setJobMemory "50G" . setJobTime (1*hour)) $ do
--  void $ delaunaySearchRegionPersistent delaunaySearchPoints delaunayConfig affine initialPts f
--  where
--    nmax = 6
--    affine = superIsingAffineNmax6
--    f :: V 2 Rational -> Cluster Bool
--    f deltaExts = fmap SDPB.isPrimalFeasible (remoteComputeGNYN1Bound $ bound deltaExts)
--    bound deltaExts = Bound
--      { boundKey = superIsingFeasibleDefaultGaps deltaExts nmax
--      , precision = B3d.precision (Params.block3dParamsNmax nmax)
--      , solverParams = (sdpbParamsNmax nmax) { precision = 768
--                                             , findPrimalFeasible = False }
--      , boundConfig = defaultBoundConfig
--      }
--    delaunayConfig = defaultDelaunayConfig 20 400
--    initialPts = Map.fromList $
--      [(p, Just True)  | p <- initialAllowed] ++
--      [(p, Just False) | p <- initialDisallowed]
--      where
--        initialDisallowed = []
--        initialAllowed = [toV (1.085,0.6)]



boundsProgram "GNYSigPsiEps_scaling_tests" =
  local (setJobTime (12*hour) . setJobMemory "0G" . setSlurmPartition "compute") $ void $
  doConcurrently_ $ do
  jobInfo@(nmax, procGran, jobType) <-
    [
      -- A big computation
      (18, 32, MPIJob 4 128)
      -- A medium size computation
    , (14, 2, MPIJob 1 128)
    , (14, 2, MPIJob 1 64)
    , (14, 2, MPIJob 1 32)
      -- A small computation
    , (8, 1, MPIJob 1 64)
    , (8, 1, MPIJob 1 32)
    , (8, 1, MPIJob 1 16)
    , (8, 1, MPIJob 1 8)
    ]
  -- Modify the work directory for the different bounds to include
  -- information about the job. This prevents the bounds from reading
  -- and writing the same files simultaneously.
  let
    mkFiles workDir =
      let
        jobTypeString = "jobType_" ++ sanitizeFileString (show jobInfo)
        jobTypeMarkedFiles = Bound.defaultBoundFiles (workDir </> jobTypeString)
        jsonDir' = case nmax of
          18 -> "TODO"
          14 -> "TODO"
          8  -> "TODO"
          _ -> Bound.jsonDir jobTypeMarkedFiles
      in
        jobTypeMarkedFiles { Bound.jsonDir = jsonDir' }
  pure $
    local (setJobType jobType) $
    Bound.remoteComputeWithFileTreatment Bound.keepAllFiles mkFiles (bound nmax procGran)
  where
    ngroup = 2
    mLambda = Just $ toV (0.310566, 0.14493, 0.611389, 1)
    deltaExts = toV (1.0672, 0.657, 1.74)
    bound nmax procGran = Bound
      { boundKey = gnySigPsiEpsFeasibleDefaultGaps deltaExts mLambda ngroup nmax
      , precision = B3d.precision (Params.block3dParamsNmax nmax)
      , solverParams = (Params.jumpFindingParams nmax)
        { precision = 768
        , procGranularity = procGran
        }
      , boundConfig = defaultBoundConfig
      }

boundsProgram p = unknownProgram p



--gny4IslandPts :: Map.Map (V 3 Rational) (Maybe Bool)
--gny4IslandPts = Map.fromList $
--      [(p, Just True)  | p <- initialAllowed] ++
--      [(p, Just False) | p <- initialDisallowed]
--      where
