#!/bin/bash

module load cmake/3.10.2 gcc/7.3.0 boost/1_68_0-gcc730 eigen/eigen
ulimit -c unlimited
echo /home/dssimmon/install/bin/blocks_3d $@
/home/dssimmon/install/bin/blocks_3d $@
