#!/bin/bash

module load cmake/3.10.2 gcc/7.3.0 openmpi/3.0.0 boost/1_68_0-gcc730
#echo srun /home/dssimmon/install/bin/sdpb $@
#srun /home/dssimmon/install/bin/sdpb $@
echo mpirun --mca btl vader,openib,self /home/dssimmon/install/bin/sdpb $@
mpirun --mca btl vader,openib,self /home/dssimmon/install/bin/sdpb $@
