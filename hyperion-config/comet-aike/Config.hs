-- To use, create a symlink 'src/Config.hs' pointing to this file

{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TemplateHaskell   #-}

module Config where

import           Data.FileEmbed          (makeRelativeToProject, strToExp)
import           Hyperion
import qualified Hyperion.Slurm as Slurm

scriptsDir :: FilePath
scriptsDir = $(makeRelativeToProject "comet-aike" >>= strToExp)

config :: HyperionConfig
config = (defaultHyperionConfig "/oasis/scratch/comet/aikeliu/temp_project/hyperion")
  { emailAddr = Just "aliu7@caltech.edu"
  , defaultSbatchOptions = Slurm.defaultSbatchOptions
    { Slurm.partition = Just "compute" }
  }

staticConfig :: HyperionStaticConfig
staticConfig = defaultHyperionStaticConfig