-- To use, create a symlink 'src/Config.hs' pointing to this file

{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TemplateHaskell   #-}

module Config where

import           Data.FileEmbed          (makeRelativeToProject, strToExp)
import           Hyperion
  
flatSpaceFunctionalsDir :: FilePath
flatSpaceFunctionalsDir = "/central/groups/dssimmon/yuezhou/flat-space-functionals"

scriptsDir :: FilePath
scriptsDir = $(makeRelativeToProject "caltech-hpc-yuezhou" >>= strToExp)

config :: HyperionConfig
config = (defaultHyperionConfig "/central/groups/dssimmon/yuezhou/hyperion3")
  { emailAddr = Just "yzli@caltech.edu" }

staticConfig :: HyperionStaticConfig
staticConfig = defaultHyperionStaticConfig