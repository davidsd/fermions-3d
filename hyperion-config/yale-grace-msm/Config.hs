-- To use, create a symlink 'src/Config.hs' pointing to this file

{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TemplateHaskell   #-}

module Config where

import           Data.FileEmbed (makeRelativeToProject, strToExp)
import           Hyperion
import qualified Hyperion.Slurm as Slurm

scriptsDir :: FilePath
scriptsDir = $(makeRelativeToProject "yale-grace-msm" >>= strToExp)

config :: HyperionConfig
config = (defaultHyperionConfig "/home/msm233/scratch60/hyperion-data/")
  { emailAddr             = Just "matthew.mitchell@yale.edu"
  , defaultSbatchOptions = Slurm.defaultSbatchOptions
    { Slurm.partition = Just "pi_poland,day" }
  }
