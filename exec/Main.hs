module Main where

import           Config                         (config, staticConfig)
import           Hyperion.Bootstrap.Main        (hyperionBootstrapMain,
                                                 tryAllPrograms)
import qualified Projects.Fermions3d.FourFermions3dTest2020
import qualified Projects.Fermions3d.GNYTest2020
import qualified Projects.Fermions3d.GNYSigPsiEpsTest2020
import qualified Projects.Fermions3d.GNYPsiEpsTest2020

main :: IO ()
main = hyperionBootstrapMain config staticConfig $
  tryAllPrograms
  [ Projects.Fermions3d.FourFermions3dTest2020.boundsProgram
  , Projects.Fermions3d.GNYTest2020.boundsProgram
  , Projects.Fermions3d.GNYSigPsiEpsTest2020.boundsProgram
  , Projects.Fermions3d.GNYPsiEpsTest2020.boundsProgram
  ]
